#ifndef SECUREFLOW_MACROS_H_
#define SECUREFLOW_MACROS_H_

#include <cstdlib>
#include<iostream>

// A macro to disallow the copy constructor and operator= functions. This is
// usually placed in the private: declarations for a class.
#define SF_DISALLOW_COPY_AND_ASSIGN(TypeName)   \
  TypeName(const TypeName&) = delete;           \
  void operator=(const TypeName&) = delete;     \

#define SF_RETURNS_IF_NOT_OK(expr, msg)         \
  Status ____status = expr;                     \
  if (!____status.ok()) {                       \
    LOG::fatal(msg);                            \
    return status;                              \
  }                                             \

#define STRING_EQ(str1, str2)                   \
  std::strcmp(str1, str2) == 0                  \

#define SF_ASSERT(pred)                                                 \
  do {                                                                  \
      if(!(pred))                                                       \
        LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << "\n"; \
  } while(0)

// FIXME: find an elegant solution to variadic arguments without re-designing the logging system
#define SF_ASSERT_w_MSG1(pred, msg0)                                    \
  do {                                                                  \
      if(!(pred))                                                       \
        LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                   << msg0 << "\n";                                     \
  } while(0)

#define SF_ASSERT_w_MSG2(pred, msg0, msg1)                              \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << "\n";                               \
  } while(0)

#define SF_ASSERT_w_MSG3(pred, msg0, msg1, msg2)                        \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << msg2 << "\n";                       \
  } while(0)

#define SF_ASSERT_w_MSG4(pred, msg0, msg1, msg2, msg3)                  \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << msg2 << msg3 << "\n";               \
  } while(0)

#define SF_ASSERT_w_MSG5(pred, msg0, msg1, msg2, msg3, msg4)            \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << msg2 << msg3 << msg4 << "\n";       \
  } while(0)

#define SF_ASSERT_w_MSG6(pred, msg0, msg1, msg2, msg3, msg4, msg5)      \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << msg2 << msg3 << msg4                \
                 << msg5 << "\n";                                       \
  } while(0)

#define SF_ASSERT_w_MSG7(pred, msg0, msg1, msg2, msg3, msg4, msg5, msg6) \
  do {                                                                  \
    if(!(pred))                                                         \
      LOG(FATAL) << "SF_ASSERT failed on file:" << __FILE__ << ", line:" << __LINE__ << \
                 << msg0 << msg1 << msg2 << msg3 << msg4                \
                 << msg5 << msg6 << "\n";                               \
  } while(0)

#endif
