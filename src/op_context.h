#ifndef SECUREFLOW_OP_CONTEXT_H_
#define SECUREFLOW_OP_CONTEXT_H_

#include <vector>
#include "tensor.h"
#include "graph.h"

namespace secureflow {

  class Node;

  class OpContext {
  private:
    // Note: when this is a Variable node kernel,
    // the `input_tensor_handles_` is empty
    // When this is an Apply node kernel,
    // the `output_tensor_handles_` is empty, since it directly
    // operates on its input tensor.
    std::vector<TensorHandle> input_tensor_handles_;
    std::vector<TensorHandle> output_tensor_handles_;
    Node* node_;
  public:
    OpContext(Node* node);
    TensorHandle getInput(int idx);
    Status setOutput(int idx, TensorHandle& output);
    Status notify();
  };
}

#endif
