#ifndef SECUREFLOW_KERNEL_H_
#define SECUREFLOW_KERNEL_H_

#include "status.h"
#include "op.h"
#include "op_context.h"
#include "smc_context.h"

namespace secureflow {

  class ConvHelper;
  class Kernel {
    // The object that actual does the computation
    // This is an interface
    // The kernel is reused for multiple invokes.

  public:
    Kernel() {}
    virtual Status validateInput(OpContext* opCtx);
    virtual Status Compute(OpContext* opCtx, SMCContext* smcCtx);
    ~Kernel() {}

  };

  class AddNKernel : public Kernel {
  public:
    AddNKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~AddNKernel();
  private:
    // OTHelper* otHelper;
    // GCHelper* gcHelper;
    // MTHelper* mtHelper;
  };

  class MatMulKernel : public Kernel {
  public:
    MatMulKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~MatMulKernel();
  private:
    // Helpers are all singletons.
    // Kernels do not own them.
    // OTHelper* otHelper;
    // GCHelper* gcHelper;
    // MTHelper* mtHelper;
  };

  class Convolution2DKernel : public Kernel {
  public:
    Convolution2DKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~Convolution2DKernel();
  private:
    ConvHelper* convHelper;
  };

  class FlattenKernel : public Kernel {
  public:
    FlattenKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~FlattenKernel();
  };

  class MatMulBatchKernel : public Kernel {
  public:
    MatMulBatchKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~MatMulBatchKernel();
  };

  class ReluKernel : public Kernel {
  public:
    ReluKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ReluKernel();
  };

  class ReluSoftMaxKernel : public Kernel {
  public:
    ReluSoftMaxKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ReluSoftMaxKernel();
  };

  class CrossEntropyKernel : public Kernel {
  public:
    CrossEntropyKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~CrossEntropyKernel();
  };

  class MaxPoolKernel : public Kernel {
  public:
    MaxPoolKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~MaxPoolKernel();
  };

  class ArgMaxKernel : public Kernel {
  public:
    ArgMaxKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ArgMaxKernel();
  };

  class ArgMinKernel : public Kernel {
  public:
    ArgMinKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ArgMinKernel();
  };

  class MeanKernel : public Kernel {
  public:
    MeanKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~MeanKernel();
  };

  class ReconstructKernel : public Kernel{
    // This kernel is used solely for node consistency.
    // E.g. user don't have to manually call smcCtx->reconstruct()
    // to obtain the reconstructed value, just plug a node with op
    // with this kernel where needed.
  public:
    ReconstructKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ReconstructKernel();
  };

  class ExponentKernel : public Kernel {
  public:
    ExponentKernel();
    Status validateInput(OpContext* opCtx);
    Status Compute(OpContext* opCtx, SMCContext* smcCtx) override;
    ~ExponentKernel();
  };

  // Public Kernels
  // Note: if all the class declarations look the same, we should be
  // using directive to save effort.

  // Define macros

#define REGISTER_KERNEL_WITH_OP_NAME(name, kernelClass) \
  KernelRegistry::Global().addEntry(name, kernelClass)

#define SUM_VECTOR_TO_SCALAR(vecTensor, scalarTensor)                   \
  Check_EQ(vecTensor->shape->rank(), 1) << "SUM_VECTOR_TO_SCALAR: "     \
  << "vectorTensor "##vecTensor "has none-one rank.";                   \
  Check_EQ(scalarTensor->shape->rank(), 0) << "SUM_VECTOR_TO_SCALAR: "  \
  << "scalarTensor "##scalarTensor "has none-zero rank.";               \
  for(int i = 0; i < vecTensor->shape->dim_at(0); ++i) {                \
    scalarTensor[{}] += vecTensor[{i}];                                 \
  }                                                                     \

#define START_A2YY2A_PROTOCOL_FOR_FUNC(circuitFunc) \
  2YY2ACircuitBuilderWrapper(circuitFunc)

}

#endif
