#include "util.h"

namespace secureflow {

  std::vector<std::string> str_split(std::string s, const char* delim) {
    char str[s.size() + 1];
    strcpy(str, s.c_str());
    std::vector<std::string> ret;
    char* res;
    res = strtok(str, delim);
    while (res != NULL) {
      ret.push_back(std::string(res));
      res = strtok(NULL, delim);
    }
    return ret;
  }
}
