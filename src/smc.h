/*
  smc.h -- Define classes to perform secure multiparty computation.
  author: Xiaoting Tang <2018-02-22 Thu>
*/
#ifndef SMC_H
#define SMC_H

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <inttypes.h>
#include "tensor.h"

namespace secureflow {

  class SMCParty;
  class SMCClient;

  class SMCManager {
  public:
    SMCParty* garbler;
    SMCParty* evaluator;
    SMCClient* client;

    SMCManager() {}

    int init_as_client(uint8_t port);
    int init_as_garbler(const char* client_hostname, uint8_t client_port, uint8_t port);

    int init_as_evaluator(const char* client_hostname, uint8_t client_port,
                          const char* peer_hostname, uint8_t peer_port);

    /***** Garbled Circuit API *****/

    /* Aggregate the first two tensors and set the result to the third tensor */
    int gc_aggregate_2d_tensor(Tensor*, Tensor*, Tensor*);

    /* Multiply the first two matrices and set the result to the third matrix. */
    int gc_mul_2d_tensor(Tensor*, Tensor*, Tensor*);

    /***** Arithmetic Secret Sharing API *****/
    
    int ss_aggregate_2d_tensor(Tensor*, Tensor*, Tensor*);

    int ss_mul_2d_tensor(Tensor*, Tensor*, Tensor*);
  };

  class SMCParty {

  private:
    struct sockaddr_in self_addr_;
    struct sockaddr_in peer_addr_;
    struct sockaddr_in client_addr_;
    int role_;                     // server0 = 0, server1 = 1
    int peer_sock_;                // used by server1 and server 0
    int self_sock_;                // used by server0
    int client_sock_;              // used by both server0 and server1
    int ready_;                    // used by both server0 and server1
    int setup_s0_peer(uint16_t port);
    int setup_s1_peer(const char* peer_host_name, uint16_t port);
    int setup_client(const char* client_host_name, uint16_t port);
  public:
  SMCParty() : ready_(0) {}
    SMCParty(const char* peer_host_name, uint16_t port);
    SMCParty(const char* peer_host_name, uint16_t p_port, const char* client_host_name, uint16_t c_port);
    SMCParty(uint16_t port);
    SMCParty(uint16_t port, const char* client_host_name, uint16_t c_port);
    inline int32_t role() { return role_; }
    int connect_peer();
    int connect_client();
    int recv_share(int32_t& s);
    int recv_triplets(Triplet*& tri);
    int send_small(int32_t n);
    int recv_small(int32_t* n);
    int send_large(int32_t* ns, uint32_t size);
    int recv_large(int32_t* ns, uint32_t size);
  };

  class SMCClient {
  private:
    int self_sock_;
    struct sockaddr_in self_addr_;
    int server0_sock_;
    int server1_sock_;
    int ready_;
  public:
    SMCClient(){}
    SMCClient(uint16_t port);
    int wait_servers();              // Block current process until server0 and server1 connects.
    int share(int32_t n);            // Create shares of n and send to servers
    int send_triplets(uint32_t n);    // Create n triplets, split them and give them to servers
  };

  int tcp_send(int sock, int32_t n);
  int tcp_send(int sock, byte* data, uint32_t size);
  int tcp_recv(int sock, int32_t* n);
  byte* tcp_recv(int sock);
}

#endif
