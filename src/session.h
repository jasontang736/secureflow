#ifndef SECUREFLOW_SESSION_H_
#define SECUREFLOW_SESSION_H_

#include "graph.h"
#include "smc.h"

namespace secureflow {

    class Session {
    private:
        static Session* default_session_;        
    public:
        SMCManager* smc_manager;
        Session();
        static void asDefaultSession(Session* session) {
            default_session_ = session;
        }
        static Session* default_session() {
          return default_session_;
        }
        int evaluate(Graph* g, Node* n);
    };

}

#endif //SECUREFLOW_SESSION_H_
