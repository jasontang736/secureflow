#include "session.h"
#include "op.h"
#include <assert.h>

namespace secureflow {

    Session::Session() {
    }

    /*
     * Evaluate the node n, remember the value for evaluated node
     * Return 0 on success, return 1 if fails.
     */
    int Session::evaluate(Graph* g, Node* n) {
        assert(n->graph() == g);
        // assert(g->reacheable(n));
        if (!n->dirty()) {
            EdgeSet* in_edge = n->in_edges();
            for (auto it = in_edge->begin(); it != in_edge->end(); ++it) {
                if (evaluate(g, (*it)->src()) != 0) {
                    throw std::runtime_error("Evaluation failed.");
                }
            }
            return executor()->execute(n);
        } else {
            return 0;
        }
    }


    int Executor::execute(Node* n) {
        switch (executorType_) {
        case Debug:
            return n->op()->debug_kernel()(n->op()->execContext());
        case SMC_GC_Only:
            return n->op()->smc_gc_only_kernel()(n->op()->execContext());
        case SMC_SS_Only:
            return n->op()->smc_ss_only_kernel()(n->op()->execContext());
        case SMC_GC_SS:
            return n->op()->smc_gc_ss_kernel()(n->op()->execContext());
        }
        return -1;
    };
}
