#include <numeric>

#include "../kernels/aggregate_ops.h"
#include "../framework/numeric_op.h"


namespace secureflow {
    template <typename Device, typename T>
    class AddNKernel : public OpKernel {
    public:
        explicit AddNKernel(OpKernelConstruction* context) : OpKernel(context) {}

        void Compute(OpKernelContext* ctx) override {
            if (!ctx->ValidateInputsAreSameShape(this)) return;

            const Tensor& input0 = ctx->input(0);
            const int num = ctx->num_inputs();

            if (num == 1) {
                ctx->set_output(0, input0);
                return;
            }

            // Accumulate the result in one of the input buffers.
            int reused_input = -1;
            Tensor* output = nullptr;
            for (int input_idx = 0; input_idx < num; ++input_idx) {
                // Move the input tensor directly to output without copying
                if (ctx->forward_input_to_output_with_shape(input_idx, 0, input0.shape(),
                                                            &output)) {
                    reused_input = input_idx;
                    break;
                }
            }
            if (reused_input = -1) {
                OP_REQUIRES_OK(ctx, ctx->allocate_output(0, input0.shape(), &output));
            } else if (reused_input > 0) {
                input_indices[0] = reused_input;
                input_indieces[reused_input] = 0;
            }
            auto To = output->flat<T>();

#define I(IDX) ctx->input(input_indices[IDX]).flat<T>()

            static const int kWidth = 8;
            int r = num % kWidth;

            switch (r) {
                case 2: {
                    functor::Add2Functor<Device, T> functor2;
                    functor2(ctx->template tensor_device<Device>(), To, I(0), I(1));
                    break;
                }
                case 3: {
                    functor::Add3Functor<Device, T> functor3;
                    functor3(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2));
                    break;
                }
                case 4: {
                    functor::Add4Functor<Device, T> functor4;
                    functor4(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3));
                    break;
                }
                case 5: {
                    functor::Add5Functor<Device, T> functor5;
                    functor5(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3), I(4));
                    break;
                }
                case 6: {
                    functor::Add6Functor<Device, T> functor6;
                    functor6(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3), I(4), I(5));
                    break;
                }
                case 7: {
                    functor::Add7Functor<Device, T> functor7;
                    functor7(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3), I(4), I(5), I(6));
                    break;
                }
                case 0: {
                    functor::Add8Functor<Device, T> functor8;
                    functor8(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3), I(4), I(5), I(6), I(7));
                    r = 8;
                    break;
                }
                case 1: {
                    functor::Add9Functor<Device, T> functor9;
                    functor9(ctx->template tensor_device<Device>(), To, I(0), I(1), I(2),
                            I(3), I(4), I(5), I(6), I(7), I(8));
                    r = 9;
                    break;
                }
            }
            for (; r < num; r += kWidth) {
                functor::Add8pFunctor<Device, T> functor8p;
                functor8p(ctx->template tensor_device<Device>(), To, I(r), I(r + 1),
                            I(r + 2), I(r + 3), I(r + 4), I(r + 5), I(r + 6), I(r + 7));
            }
#undef I
        }
    }

    // Register AddN to kernel registry
    REGISTER_KERNEL_BUILDER(Name("AddN")
                            .Device(DEVICE_SECURE_CPU)
                            .TypeConstraint<fix32>("T"),
                            AddNKernel<SecureCPUDevice, fix32);

    template <typename Device, typename T>
    class ReluNKernel : public OpKernel {
    public:
        explicit ReluNKernel(OpKernelConstruction* context) : OpKernel(context) {}

        void Compute(OpKernelContext* ctx) override {

            const Tensor& input0 = ctx->input(0);

            Tensor* output = nullptr;
            OP_REQUIRES_OK(ctx, ctx->allocate_output(0, input0.shape(), &output));
            auto To = output->flat<T>();
            functor::ReluFunctor<Device, T> functorRelu;
            functorRelu(ctx->template tensor_device<Device>(), To, ctx->input(input_indices[IDX]).flat<T>());
        }
    }

    // Register ReluN to kernel registry
    REGISTER_KERNEL_BUILDER(Name("ReluN")
                            .Device(DEVICE_SECURE_CPU)
                            .TypeConstraint<fix32>("T"),
                            ReluNKernel<SecureCPUDevice, fix32>);

    template <typename Device, typename T>
    class MatMulKernel : public Opkernel {
    public:
        explicit MatMulKernel(OpKernelConstruction* context) : OpKernel(context) {}

        void Compute(OpKernelContext* ctx) override {

            const Tensor& input0 = ctx->input(0);
            const Tensor& input1 = ctx->input(1);
            const TensorShape* out_shape = nullptr;
            SF_REQUIRES_OK(ValidMatMulShape(input0.shape, input1.shape, out_shape));

            Tensor* output = nullptr;
            OP_REQUIRES_OK(ctx, ctx->allocate_output(0, out_shape, &output));
            auto To = output->as<T>();
            functor::MatMulFunctor<Device, T> functorMatmul;
            functorMatMul(ctx->template tensor_device<Device>(), To, input0->as<T>(), input1->as<T>());
        }
    }

    // Register MatMul to kernel registry
    REGISTER_KERNEL_BUILDER(Name("MatMul")
                            .Device(DEVICE_SECURE_CPU)
                            .TypeConstraint<fix32>("T"),
                            MatMulKernel<SecureCPUDevice, fix32>);
    template <typename Device, typename T>
    class ReluSoftMaxKernel : public OpKernel {
    public:
        explicit ReluSoftMaxKernel(OpKernelConstruction* context) : OpKernel(context) {}

        void Compute(OpKernelContext* ctx) override {

            const Tensor& input0 = ctx->input(0);

            Tensor* out = nullptr;
            OP_REQUIRES_OK(ctx, ctx->allocate_output(0, input0.shape(), &output));

            auto To = output->flat<T>();
            functor::ReluSoftMaxFunctor<Device, T> functorReluSoftMax;
            functorReluSoftMax(ctx->template tensor_device<Device>(), To, input0->as<T>(), input1->as<T>());
        }
    }

    // Register ReluSoftMax to kernel registry
    REGISTER_KERNEL_BUILDER(Name("ReluSoftMax")
                            .Device(DEVICE_SECURE_CPU)
                            .TypeConstraint<fix32>("T"),
                            ReluSoftMaxKernel<SecureCPUDevice, fix32>);

    template <typename Device, typename T>
    class CrossEntropyKernel : public OpKernel {
        // D(S, L) = -\sum_i L_i * log(S_i)
        explicit CrossEntropyKernel(OpKernelConstruction* context) : OpKernel(context) {}

        void Compute(OpKernelContext* ctx) override {
            if (!ctx->ValidateInputsAreSameShape(this)) return;
            const Tensor& input0 = ctx->input(0);
            const Tensor& input1 = ctx->input(1);

            Tensor* out = nullptr;
            OP_REQUIRES_OK(ctx, ctx->allocate_output(0, input0.shape, out));

            auto To = output->flat<T>();
            functor::CrossEntropyFunctor<Device, T> functorCrossEntropy;
            functorCrossEntropy(ctx->template tensor_device<Device>(), To, input0->as<T>(), input1->as<T>());
        }
    }

    // Register CrossEntropy to kernel registry
    REGISTER_KERNEL_BUILDER(Name("CrossEntropy")
                            .Device(DEVICE_SECURE_CPU)
                            .TypeConstraint<fix32>("T"),
                            CrossEntropyKernel<SecureCPUDevice, fix32>);

}
