#include <stdlib.h>
#include <cstdio>
#include <time.h>
#include "logging.h"
#include "env_time.h"
#include "types.h"

namespace secureflow {
  LogMessage::LogMessage(const char* fname, int line, int severity)
    : fname_(fname), line_(line), severity_(severity) {}

  void LogMessage::GenerateLogMessage() {
    static EnvTime* env_time = secureflow::EnvTime::Default();
    uint64 now_micros = env_time->NowMicros();
    time_t now_seconds = static_cast<time_t>(now_micros / 1000000);
    int32 micros_remainder = static_cast<int32>(now_micros % 1000000);
    const size_t time_buffer_size = 30;
    char time_buffer[time_buffer_size];
    strftime(time_buffer, time_buffer_size, "%Y-%m-%d %H:%M:%S",
             localtime(&now_seconds));
    fprintf(stderr, "%s.%06d: %c %s:%d] %s\n", time_buffer, micros_remainder,
            "IWEF"[severity_], fname_, line_, str().c_str());
  }

  void LogMessage::~LogMesssgae() {
    GenerateLogMessage();
  }

  LogMessageFatal::LogMessageFatal(const char* file, int line)
    : LogMessage(file, line, FATAL) {}

  LogMessageFatal::~LogMessageFatal() {
    // abort() ensures we don't return (we promised we would not via
    // ATTRIBUTE_NORETURN).
    GenerateLogMessage();
    abort();
  }

  void LogString(const char* fname, int line, int severity,
                 const string& message) {
    LogMessage(fname, line, severity) << message;
  }

  template <>
  void MakeCheckOpValueString(std::ostream* os, const char& v) {
    if (v >= 32 && v <= 126) {
      (*os) << "'" << v << "'";
    } else {
      (*os) << "char value " << static_cast<short>(v);
    }
  }

  template <>
  void MakeCheckOpValueString(std::ostream* os, const signed char& v) {
    if (v >= 32 && v <= 126) {
      (*os) << "'" << v << "'";
    } else {
      (*os) << "signed char value " << static_cast<short>(v);
    }
  }

  template <>
  void MakeCheckOpValueString(std::ostream* os, const unsigned char& v) {
    if (v >= 32 && v <= 126) {
      (*os) << "'" << v << "'";
    } else {
      (*os) << "unsigned char value " << static_cast<unsigned short>(v);
    }
  }

  CheckOpMessageBuilder::CheckOpMessageBuilder(const char* exprtext)
    : stream_(new std::ostringstream) {
    *stream_ << "Check failed: " << exprtext << " (";
  }

  CheckOpMessageBuilder::~CheckOpMessageBuilder() { delete stream_; }

  std::ostream* CheckOpMessageBuilder::ForVar2() {
    *stream_ << " vs. ";
    return stream_;
  }

  string* CheckOpMessageBuilder::NewString() {
    *stream_ << ")";
    return new string(stream_->str());
  }
}
