#include "network.h"

namespace secureflow {

  MailMan::MailMan() {
    singleton = MailMan();
  }

  static MailMan* Global() {
    return &singleton;
  }
}
