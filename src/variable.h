#ifndef SECUREFLOW_VARIABLE_H
#define SECUREFLOW_VARIABLE_H

#include "graph.h"

namespace secureflow {

  class Node;
  class Variable {
    // Variable is a simple wrapper of the Node class
  private:
    Node* n_;
  public:
    Variable(Node* n) : n_(n) {}
    Node* node() { return n_; }
  };

}

#endif //SECUREFLOW_VARIABLE_
