#ifndef SECUREFLOW_TYPES_H_
#define SECUREFLOW_TYPES_H_

#include <inttypes.h>
#include <math.h>
#include <string>

namespace secureflow {

  using std::string;

  typedef uint16_t uint16;
  typedef uint32_t uint32;
  typedef uint64_t uint64;
  typedef int16_t int16;
  typedef int32_t int32;
  typedef int64_t int64;

  inline bool test_bit(int16_t n, int pos) {
    return ((n & (1 << pos)) == 1);
  }

  inline float fract(int16_t n) {
    // interpret the bit values in n as fractions
    // E.g. 1110111 > 1*0.5 + 1 * 0.5^2 + 1 * 0.5^3 + 0 * 0.5 ^ 4
    float acc = 0.0;
    int exp = 1;
    while (exp <= 16) {
      acc += test_bit(n, 16 - exp) ? pow(0.5, exp++) : 0;
    }
    return acc;
  }

  typedef struct fix32 {
    int32_t buf;
    float interpret() {
      return (buf >> 16) + fract((buf << 16) >> 16);
    }
    fix32 operator+(fix32 rhs) {
      this->buf += rhs.buf;
      return *this;
    }
    fix32 operator-(fix32 rhs) {
      this->buf -=  rhs.buf;
      return *this;
    }
    fix32 operator=(fix32 rhs) {
      this->buf = rhs.buf;
      return *this;
    }
  } fix32 ;

}

#endif
