#include <string>
#include <cstdlib>
#include "graph.h"
#include "string_util.h"

namespace secureflow {

  Node::Node(std::shared_ptr<NodeDef> nodeDef)
    : name_(nodeDef.name()), op_(nodeDef.op()),
  {
    if (nodeDef.id() == 0 && (std::strcmp(name_.c_str(), "Source") != 0)) {
      id_ = -1; // uninitialized id
    } else {
      id_ = nodeDef.id();
    }

    if (STRING_EQ(op_.c_str(), "Source")) {
      nodeType_ = NT_Source;
    }
    else if (STRING_EQ(op_.c_str(), "Sink")) {
      nodeType_ = NT_Sink;
    }
    else if (STRING_EQ(op_.c_str(), "Variable")) {
      nodeType_ = NT_Variable;
    }
    else if (STRING_EQ(op_.c_str(), "Constant")) {
      nodeType_ = NT_Constant;
    }
    else if (STRING_EQ(op_.c_str(), "Placeholder")) {
      nodeType_ = NT_Placeholder;
    }
    else {
      nodeType_ = NT_Op;
    }
    nDef_ = nodeDef;
  }

      Node::~Node() {}

  std::shared_ptr<NodeDef> Node::Def() {
    return nDef_;
  }

  NeighborIter Node::in_neighbors() {
    return NeighborIter(this, true);
  }

  NeighborIter Node::out_neighbors() {
    return NeighborIter(this, false);
  }

  int Node::in_neighbor_size() {
    return in_edges_.size();
  }

  int Node::out_neighbor_size() {
    return out_edges_.size();
  }

  void Node::set_grad_node(Node* grad_node) {
    SF_ASSERT(this != grad_node);
    grad_node_ = grad_node;
  }

  // Edge
  Edge::~Edge() {}


  // Graph
  Graph::Graph() {
    source_ = new Node;
    sink_ = new Node;
    this->addEdge(source_, sink_, 0);
  }

  Graph::Graph(OpRegistry && op_registry)
    : op_registry_(&op_registry), id_counter_(0) {
    source_ = new Node;
    sink_ = new Node;
    this->addEdge(source_, sink_, 0);
  }

  Graph::~Graph() {
    for (Node* n : nodes_) {
      delete n;
    }
    for (Edge* e : edges_) {
      delete e;
    }
  }

  Node* Graph::simpleAddNode(const NodeDef& nodeDef) {
    Node* n = new Node(nodeDef);
    n->graph_ = this;
    return n;
  }

  void Graph::connectNodeWithIncomingNeighbors(Node* n, const NodeDef& nodeDef) {
    // Require input nodes to be in graph
    for (auto input = nodeDef.inputs().begin();
         input != nodeDef.inputs().end(); input++) {
      std::vector<std::string> input_slices = StrSlice(*input, ':');
      std::string node_name_id = input_slices[0];
      int outport = Str2Int(input_slices[1]);
      int inport = Str2Int(input_slices[2]);
      int node_id = Str2Int(input_slices[3]);
      Node* nd = this->getNodeById(node_id);
      SF_REQUIRES_NOT_NULL(nd, "getNodeById() returns null pointer.");

      this->addEdge(nd, n, outport, inport);
    }
  }

  Node* Graph::addNode(const NodeDef& nodeDef) {
    Node* n = simpleAddNode(nodeDef);
    connectNodeWithIncomingNeighbors(n, nodeDef);
    return n;
  }

  Node* Graph::getNodeById(int id) {
    return nodes_[id];
  }

  Edge* Graph::addEdge(Node* src, Node* dst, int src_output, int dst_input) {
    Edge* e = new Edge(src, dst, src_output, dst_input);
    edges_.push_back(e);

    src->out_edges_.push_back(e);
    dst->in_edges_.push_back(e);
    return e;
  }

  Edge* Graph::addEdge(Node* src, Node* dst, int src_output) {
    int dst_input = dst->in_edges_.size();
    Edge* e = new Edge(src, dst, src_output, dst_input);
    edges_.push_back(e);

    src->out_edges_.push_back(e);
    dst->in_edges_.push_back(e);
    return e;
  }

  void Graph::newNameId(std::string & name, int64 & id) {
    id = id_counter_++;
    name = StrCat(name, '_', id_counter_);
  }

  std::unique_ptr<GraphDef> Graph::serializeToGraphDef() {
    std::unique_ptr<GraphDef> gDef = std::unique_ptr<GraphDef>(new GraphDef());

    // *gDef->mutable_op_registry() = op_registry_->Def();

    // Should be topologically sorted
    for (Node* n : nodes_) {
      (*gDef->mutable_nodes())[n->id()] = n->Def();
    }

    return gDef;
  }

  std::unique_ptr<Graph> Graph::readFromGraphDef(GraphDef& gDef) {
    std::unique_ptr<Graph> g = std::unique_ptr<Graph>(new Graph(OpRegistry(gDef.op_registry())));
    for (auto p = gDef.nodes().begin();
         p != gDef.nodes().end(); p++) {
      g->simpleAddNode(p->second);
    }
    for (auto n = g->nodes_.begin(); n != g->nodes_.end(); n++) {
      const NodeDef& nd = gDef.nodes().at((*n)->id());
      g->connectNodeWithIncomingNeighbors(*n, nd);
    }
    return g;
  }

}
