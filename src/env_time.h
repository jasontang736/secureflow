#ifndef SECUREFLOW_ENV_TIME_H
#define SECUREFLOW_ENV_TIME_H

#include <stdint.h>
#include "types.h"
#include <sys/time.h>

namespace secureflow {

     class EnvTime {
     public:
          EnvTime() {}
          ~EnvTime() = default;

          static EnvTime* Default();

          uint64 NowMicros() {
               struct timeval tv;
               gettimeofday(&tv, nullptr);
               return static_cast<uint64>(tv.tv_sec) * 1000000 + tv.tv_usec;
          }

          uint64 NowSeconds() {
               return NowMicros() / 1000000L;
          }
     };
}

#endif //SECUREFLOW_ENV_TIME_H
