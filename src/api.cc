#include "api.h"
#include <stdexcept>

namespace secureflow {

  Node* secflow_get_source_node(Graph* g) {
    assert(g);
    return g->source();
  }

  Node* secflow_get_sink_node(Graph* g) {
    assert(g);
    return g->sink();
  }

  Node* secflow_create_placeholder(Graph* g, int* shape, int size) {
    assert(g);
    Shape s;
    for (int i = 0; i < size; ++i) {
      s.add_dim(shape[i]);
    }

    return g->create_placeholder(&s);
  }

  Node* secflow_create_constant(Graph* g, float v) {
    assert(g);
    return g->create_constant(v);
  }

  Node* secflow_square(Node* n) {
    assert(n);
    Graph* g = n->graph();
    Node* node = g->applyOp(n, "Square");
    return node;
  }

  /*
   * Multiply two tensor
   */
  Node* secflow_matmul(Node* a, Node* b) {
    assert(a && b);
    assert(a->graph() == b->graph());      // Require two nodes are from the same graph
    Graph* g = a->graph();
    Node* n = g->applyOp(a, b, "MatMul");
    return n;
  }

  /*
   * Summing up tensors
   */
  Node* secflow_sum(Node* a, Node* b) {
    assert(a && b);
    assert(a->graph() == b->graph());      // Require two nodes are from the same graph
    Graph* g = a->graph();
    Node* n = g->applyOp(a, b, "Sum");
    return n;
  }

  /*
   * Evaluate a node
   */
  Tensor* secflow_evaluate(Session* sess, Graph* g, Node* n) {
    assert(sess && g && n);
    assert(n->graph() == g);
    if (sess->evaluate(g, n) != 0) {
      throw std::runtime_error("Evaluation failed.");
    }
    return n->value();
  }

  // Node* secflow_create_node(Graph* g, string op_name) {
  //     Op* op = g->getOpByName(op_name);
  // } // Variable* Variable(std::vector<int> shape) {
  //     // Do not allocate memory for tensor content
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     // Allocate new OpDef using copy constructor
  //     OpDef* opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("Variable"));
  //     auto* attr_map = opdef->mutable_attr();
  //     auto iterator = attr_map->find("T");
  //     SF_ASSERT(iterator != attr_map->end());

  //     // Set shape to NodeDef
  //     AttrValue attr_value;
  //     TensorShapeProto shape_proto;
  //     for (int i = 0; i < shape.size(); ++i) {
  //         shape_proto.add_size(i);
  //     }
  //     iterator->second.set_shape(shape_proto);

  //     std::shared_ptr<NodeDef> nodedef = std::make_shared<NodeDef>();
  //     *(nodedef->mutable_op()) = *opdef;

  //     std::string name = std::string("Variable");
  //     int64 id;
  //     g->newNameId(name, id);
  //     nodedef->set_name(name);
  //     nodedef->set_id(id);

  //     Node* n = g->simpleAddNode(*nodedef);

  //     // Each Variable/Constant/Placeholder node is connected to the source node
  //     g->addEdge(g->source(), n, 0);

  //     Variable* v = new Variable(n);
  //     return v;
  // }

  // Variable* MatMul(Variable* a, Varible* b) {
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     // Allocate new OpDef using copy constructor
  //     OpDef* opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("MatMul"));

  //     std::shared_ptr<NodeDef> nodedef = std::make_shared<NodeDef>();
  //     *(nodedef->mutable_op()) = *opdef;

  //     std::string name = std::string("MatMul");
  //     int64 id;
  //     g->newNameId(name, id);
  //     nodedef->set_name(name);
  //     nodedef->set_id(id);

  //     Node* n = g->simpleAddNode(*nodedef);
  //     g->addEdge(a->node(), n, 0, 0);
  //     g->addEdge(b->node(), n, 0, 1);
  //     Variable* v = new Variable(n);

  //     return v;
  // }

  // // h = sf.Conv2d(sources, filters)
  // Variable* Conv2d(Variable* sources, Variable* filters) {
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     OpDef* opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("Conv2d"));

  //     std::shared_ptr<NodeDef> nodedef = std::make_shared<NodeDef>();
  //     *(nodedef->mutable_op()) = *opdef;

  //     std::string name = std::string("Conv2d");
  //     int64 id;
  //     g->newNameId(name, id);
  //     nodedef->set_name(name);
  //     nodedef->set_id(id);

  //     Node* n = g->simpleAddNode(*nodedef);
  //     g->addEdge(sources->node(), n, 0, 0);
  //     g->addEdge(filters->node(), n, 0, 1);
  //     Variable* v = new Variable(n);

  //     return v;
  // }

  // // h = sf.MaxPool(sources, stride)
  // Variable* MaxPool(Variable* sources, Variable* stride) {
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     OpDef* opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("MaxPool"));

  //     std::shared_ptr<NodeDef> nodedef = std::make_shared<NodeDef>();
  //     *(nodedef->mutable_op()) = *opdef;

  //     std::string name = std::string("MaxPool");
  //     int64 id;
  //     g->newNameId(name, id);
  //     nodedef->set_name(name);
  //     nodedef->set_id(id);

  //     Node* n = g->simpleAddNode(*nodedef);
  //     g->addEdge(sources->node(), n, 0, 0);
  //     g->addEdge(stride->node(), n, 0, 1);
  //     Variable* v = new Variable(n);

  //     return v;
  // }

  // // h = sf.Relu(sources)
  // Variable* Relu(Variable* sources, Variable* stride) {
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     OpDef* opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("Relu"));

  //     std::shared_ptr<NodeDef> nodedef = std::make_shared<NodeDef>();
  //     *(nodedef->mutable_op()) = *opdef;

  //     std::string name = std::string("Relu");
  //     int64 id;
  //     g->newNameId(name, id);
  //     nodedef->set_name(name);
  //     nodedef->set_id(id);

  //     Node* n = g->simpleAddNode(*nodedef);
  //     g->addEdge(sources->node(), n, 0, 0);
  //     g->addEdge(stride->node(), n, 0, 1);
  //     Variable* v = new Variable(n);

  //     return v;
  // }

  // // h = sf.SoftmaxCrossEntropyLoss(predictions, truths)
  // Variable* ReluSoftMaxCrossEntropyLoss(Variable* preds, Variable* truths) {
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     // ReluSoftmax
  //     OpDef* softmax_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("ReluSoftmax"));

  //     std::shared_ptr<NodeDef> softmax_nodedef = std::make_shared<NodeDef>();

  //     *(softmax_nodedef->mutable_op()) = *softmax_opdef;

  //     std::string name = std::string("ReluSoftmax");
  //     int64 id;
  //     g->newNameId(name, id);
  //     softmax_nodedef->set_name(name);
  //     softmax_nodedef->set_id(id);

  //     Node* softmax_n = g->simpleAddNode(*softmax_nodedef);
  //     g->addEdge(preds->node(), softmax_n, 0, 0);
  //     Variable* softmax_preds = new Variable(softmax_n);

  //     // CrossEntropy
  //     OpDef* cross_entropy_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("CrossEntropy"));

  //     std::shared_ptr<NodeDef> cross_entropy_nodedef = std::make_shared<NodeDef>();

  //     *(cross_entropy_nodedef->mutable_op()) = *cross_entropy_opdef;
  //     name = std::string("CrossEntropy");
  //     g->newNameId(name, id);
  //     cross_entropy_nodedef->set_name(name);
  //     cross_entropy_nodedef->set_id(id);

  //     Node* cross_entropy_n = g->simpleAddNode(*cross_entropy_nodedef);
  //     g->addEdge(softmax_preds->node(), cross_entropy_n, 0, 0);
  //     g->addEdge(truths->node(), cross_entropy_n, 0, 1);
  //     Variable* loss = new Variable(cross_entropy_n);

  //     return n;
  // }

  // // step = gradientDescentOptimizer.minimize(loss)
  // Variable* GradientDescentOptimizer::minimize(Variable* loss) {
  //     // Return a variable, whose node_ is a associated with an `Apply` op
  //     // This op applys the gradient to each node associated with `Variable` op
  //     Graph* g = Session::Default()->getDefaultGraph();

  //     // Build gradient graph.
  //     // TODO wrap the following function.
  //     Node* loss_node = loss->node();

  //     return Session::Default()->getGradientDescentOptimizer()->optimize(loss, true);
  // }

  // TensorHandle Variable::eval(std::map<std::string, TensorHandle> & feed_dict) {
  //     // Initialize named tensor and start executing
  //     Graph* g = Session::Default()->getDefaultGraph();
  //     Allocator* a = Session::Default()->getDefaultAllocator();
  //     Executor* e = Session::Default()->getDefaultExecutor();

  //     for (auto & it = g->nodes_.begin(); it != g->nodes_.end(); ++it) {
  //         Node* n = *it;
  //         if (!it->is_placeholder()) // Only fill placeholder
  //             continue;

  //         if (feed_dict.find(n->name()) = feed_dict.end()) {
  //             LOG(FATAL) << "feed_dict is incomplete." \
  //                        << "Tensor with node name " << n->name() \
  //                        << "is not provided.";
  //         }

  //         std::shared_ptr<NodeDef> ndef = n->Def();
  //         *(ndef->mutable_outputtensors()->Add()) = a->AllocateTensorFor(feed_dict[n->name()])
  //             }

  //     e->execute();

  //     return TensorHandle(this->node()->Def()->outputtensors()[0]);
  // }


  /* SMC related API */

  /*
   * Reconstruct Additive Share
   * The order of sending and receiving is:
   *   server recv first, client send first
   *   server send second, client recv second
   */
  int secflow_smc_rec(int32_t xi, int32_t& x, SMCParty* self) {
    int32_t x_peer;
    if (self->role() == 0) {
      // server
      self->recv_small(&x_peer);
      self->send_small(xi);
    } else {
      // client
      self->send_small(xi);
      self->recv_small(&x_peer);
    }
    x = x_peer + xi; // Reconstruction
    return 0;
  }

  /*
   * Compute Beaver's multiplication
   * This is api for one party.
   */
  int secflow_smc_beaver_mul(int32_t x0i, int32_t x1i, Triplet* tri, int32_t& yi, SMCParty* peer) {
    int32_t ui, vi, zi, e, f;
    tri->getu(0, ui);
    tri->getv(0, vi);
    tri->getz(0, zi);
    int32_t ei = x0i - ui;
    int32_t fi = x1i - vi;
    secflow_smc_rec(ei, e, peer);
    secflow_smc_rec(fi, f, peer);
    int32_t role = peer->role();
    yi = -role * e * f + x0i * f + e * x1i + zi;
    printf("ui: %d, vi: %d, zi: %d\n", ui, vi, zi);
    printf("e: %d, f: %d, yi: %d, x0i: %d, x1i: %d, zi: %d\n",
           e, f, yi, x0i, x1i, zi);

    return 0;
  }

}  // secureflow
