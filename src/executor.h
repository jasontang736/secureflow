#ifndef SECUREFLOW_EXECUTOR_H_
#define SECUREFLOW_EXECUTOR_H_

#include "status.h"
#include "graph_manager.h"
#include "graph.h"
#include "op.h"
#include "smc.h"
#include "thread_pool.h"
#include <functional>
#include <deque>

namespace secureflow {

    class Executor {
    public:

        Executor(GraphManager* gm); // owns gm
        ~Executor();
        Status execute();
        Status addToReadyQueue(Node* n);

    private:

        GraphManager* gm_;
        ThreadPool threadPool_;
        unsigned numThreadSupported_;
        bool finished_;

        SF_DISALLOW_COPY_AND_ASSIGN(Executor);
    }
}

#endif
