#include "executor.h"
#include <vector>

namespace secureflow {

    enum Stage {
        sForward,
        sBackward,
        sApply,
    };

    Executor::Executor(GraphManager* gm) : gm_(gm) {
        gm.setStage(sForward);
        numThreadSupported_ = std::thread::hardware_concurrency();
        if (numThreadSupported_ < 1) {
            Log(ERROR) << "hardware_concurrency() returns " << numThreadSupported_
                       << ", use 4 as default value\n";
            numThreadSupported_ = 4;
        }

    }

    Status addToReadyQueue(Node* n) {
        threadPool.enqueue(KernelRegistry::Global()->getKernelByOpName(n->opName()),
                           n->getOpContext(), n->getSMCContext());
        return Status::OK();
    }

    Status Executor::execute() {
        threadPool_ = ThreadPool(numThreadSupported_);

        std::shared_ptr<Node> source = gm_->source();
        threadPool.enqueue(KernelRegistry::Global()->getKernelByOpName(source->opName()),
                           source->getOpContext(), source->getSMCContext());

        return Status::OK();
    }

    bool Executor::finished() {
        finished_ = true;
        return finished_;
    }
}
