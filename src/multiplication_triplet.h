#ifndef SECUREFLOW_MULTIPLICATION_TRIPLET_H_
#define SECUREFLOW_MULTIPLICATION_TRIPLET_H_

#include "network.h"

namespace secureflow {

  class MTHelper;
  class Triplet;
  class TripletShare;
  class TripletShareProto;

  class MTHelper {
  public:
    MTHelper() {}
    static MTHelper* Global();
  private:
    MailMan mailMan;
  }
}
#endif
