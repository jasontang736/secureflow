#include "kernel.h"

namespace secureflow {


  AddNKernel::AddNKernel() {
    ///
  }

  Status AddNKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
    validateInput(opCtx, "Invalid input from AddNKernel.");

    TensorIterable ti = opCtx->getInputIterator();
    TensorHandle out = opCtx->getOutput(0);
    for (TensorIterator it = ti.begin(); it < ti.end(); ++it) {
      out += *it;  // Plain addition
    }
    opCtx->setOutput(0, out);
  }

  REGISTER_KERNEL_WITH_OP_NAME("AddN", AddNKernel);

  MatMulKernel::MatMulKernel() {
    ///
  }

  Status MatMulKernel::Compute(OpContext* opCtx, SMCContext* smcCtx ) {
    validateInput(opCtx, "Invalid input for MatMulKernel.");
    // Two graphs must sync their execution state
    int identity = smcCtx->identity();  // either 0 or 1
    if (identity != 0 && identity != 1) {
      Log(Fatal) << "Identity is neither 0 or 1, but it is " << identity;
    }
    TensorTripletsHandle mt = mtHelper->getTensorTripletsHandleById(opCtx->executingOpId());
    // Verify that mt contains enough triplet for each multiplication.
    smcCtx->validateTriplet(mt, "TripletBundle does not contain enough triplets for MatMul kernel.");

    // Compute C=A*B, where A and B are matrix
    TensorHandle A = ctx->getInput(0);
    TensorHandle B = ctx->getInput(1);
    TensorHandle C = ctx->getOutput(0);

    Triplet tri;
    Duplet eifi;
    Duplet ef;
    fix32 ci;
    for (int i = 0; i < A->shape.dim_at(0); ++i) {
      for (int j = 0; j < B->shape.dim_at(1); ++j) {
        for (int k = 0; k < A->shape.dim_at(1); ++k) {
          tri = mt->triplet_at({i, j, k});
          eifi->set(0, A[{i, k}] - tri->u());
          eifi->set(1, B[{k, j}] - tri->v());
          smcCtx->reconstruct(eifi, &ef);
          ci = -1 * identity * ef[0] * ef[1] + f * A[{i, j}] + e * B[{k, j}] + tri->z();
          C->setVal({i,j}, ci);
        }
      }
    }

    opCtx->setOutput(0, C);
    opCtx->notify();
    return Status::OK();
  }

  Convolution2DKernel::Convolution2DKernel(){
    convHelper = ConvHelper::Global();
  }

  Status Convolution2DKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
    validateInput(opCtx, "Invalid input for Convolution2DKernel.")

      int identity = smcCtx->identity();  // either 0 or 1
    if (identity != 0 && identity != 1) {
      return error::Internel("Identity is neither 0 or 1, it is ", identity);
    }
    TensorTripletsHandle mt = mtHelper->getTensorTripletsHandleById(opCtx->executingOpId());

    smcCtx->validateTriplet(mt, "TripletBundle does not contain enough triplets for MatMul kernel.");

    TensorHandle source = opCtx->getInput(0);
    TensorHandle filter = opCtx->getInput(1);
    TensorHandle output = opCtx->getOutput(0);

    int numBatch = souce->shape.dim_at(0);

    ::std::vector<int> filterSize = filter->shape.asVector();
    ::std::vector<int> stride;
    StringPiece padding;
    if(!opCtx->getAttr("stride").ok()){
      Log(Fatal) << "Unable to get attribute `stride` from OpContext for Convolution2DKernel";
    }

    if(!opCtx->getAttr("padding").ok()){
      Log(Fatal) << "Unable to get attribute `padding` from OpContext for Convolution2DKernel";

      if (padding != "SAME" && padding != "VALID") {
        Log(Fatal) << "Unknown padding scheme: " << padding;
      }

      TensorHandle paddedSource;
      TensorHandle patch;
      fix32 ci;

      Tensor ei, fi;

      if(!convHelper->padSource(source, paddedSource).ok()) {
        Log(Fatal) << "ConvHelper: Unable to pad source";
      }
      int hNumPatches = convHelper->hNumPatches(opCtx, "SAME");
      int wNumPatches = convHelper->wNumPatches(opCtx, "SAME");

      for (int b = 0; b < numBatches; ++b) {

        for (int k = 0; k < numOutChannels; ++k) {

          for (int i = 0; i < hNumPatches; ++j) {

            for (int j = 0; j < wNumPatches; ++j) {

              convHelper->getPatch(opCtx, i, j, patch);
              FlatTensorIterable patchIterable = patch->getFlatIterable();
              FlatTensorIterable filterIterable = filter->getFlatIterable();

              if (patchIterable.size() != filterIterable()) {
                Log(Fatal) << "Patch iterable has unequal size with filter/kernel iterable.";
              }

              for(::std::pair<TensorIterator, TensorIterator> \
                    it(patchIterable.begin(), filterIterable.begin());
                  it.first != patchIterable.end() && it.second != filterIterable.end();
                  ++it.first, ++it.second) {

                tri = mt->triplet_at({b, k, i, j, it.first.idx, it.second.idx});
                ei.set({b, k, i, j, it.first.idx, it.second.idx}, A[{i, k}] - tri->u());
                fi.set({b, k, i, j, it.first.idx, it.second.idx}, B[{i, k}] - tri->v());

              }
            }
          }
        }
      }

      Tensor e, f;
      fix32 ee, ff;

      smcCtx->reconstruct(ei, fi, e, f);

      for (int b = 0; b < numBatches; ++b) {

        for (int k = 0; k < numOutChannels; ++k) {

          for (int i = 0; i < hNumPatches; ++j) {

            for (int j = 0; j < wNumPatches; ++j) {

              convHelper->getPatch(opCtx, i, j, patch);
              FlatTensorIterable patchIterable = patch->getFlatIterable();
              FlatTensorIterable filterIterable = filter->getFlatIterable();

              if (patchIterable.size() != filterIterable()) {
                Log(Fatal) << ("Patch iterable has unequal size with filter/kernel iterable.");
              }

              for(::std::pair<TensorIterator, TensorIterator> \
                    it(patchIterable.begin(), filterIterable.begin());
                  it.first != patchIterable.end() && it.second != filterIterable.end();
                  ++it.first, ++it.second) {

                tri = mt->triplet_at({b, k, i, j, it.first.idx, it.second.idx});
                ee = e[{b, k, i, j, it.first.idx, it.second.idx}];
                ff = f[{b, k, i, j, it.first.idx, it.second.idx}];
                ci = -1 * identity * ee * ff + ff * A[{i, j}] + ee * B[{k, j}] + tri->z();
                output->accumulateVal({b, i, j, k}, ci);

              }
            }
          }
        }
      }

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("Convolution2D", Convolution2DKernel);

    Status FlattenKernel::FlattenKernel() {}
    Status FlattenKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for FlattenKernel.");

      TensorHandle input = opCtx->getInput(0);
      Tensor output;
      TensorIterable inputIterable = TensorIterable(input);
      TensorIterator it = inputIterable.begin();
      while (it != inputIterable.end()) {
        output.appendVal(*it);
      }
      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("Flatten", FlattenKernel);

    MatMulBatchKernel::MatMulBatchKernel() {
      mtHelper = MTHelper::Global();
    }

    Status MatMulBatchKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MatMulBatchKernel.");

      TensorHandle input = opCtx->getInput(0);
      // Two graphs must sync their execution state
      int identity = smcCtx->identity();  // either 0 or 1
      if (identity != 0 && identity != 1) {
        Log(Fatal) << "Identity is neither 0 or 1, but it is " << identity;
      }
      TensorTripletsHandle mt = mtHelper->getTensorTripletsHandleById(opCtx->executingOpId());
      // Verify that mt contains enough triplet for each multiplication.
      if (!smcCtx->validateTriplet(mt).ok()) {
        Log(Fatal) << "TripletBundle does not contain enough triplets for MatMul kernel.";
      }

      // Compute C=A*B, where A and B are matrix
      TensorHandle A = ctx->getInput(0);
      TensorHandle B = ctx->getInput(1);
      TensorHandle C = ctx->getOutput(0);

      Triplet tri;
      Tensor ei, fi, e, f;
      fix32 ee, ff, ci;

      if (A->dim_at(0) != B->dim_at(0)) {
        Log(Fatal) << "BatchMatMul have incompatible tensor: " << A.debugString()\
                   << ", and " << B.debugString();

      }
      int batchSize = A->dim_at(0);

      for (int b = 0; b < batchSize; ++b) {
        for (int i = 0; i < A->shape.dim_at(0); ++i) {
          for (int j = 0; j < B->shape.dim_at(1); ++j) {
            for (int k = 0; k < A->shape.dim_at(1); ++k) {
              tri = mt->triplet_at({i, j, k});
              ei.set({b, i, j}, A[{i, k}] - tri->u());
              fi.set({b, i, j}, B[{i, k}] - tri->v());
            }
          }
        }
      }

      smcCtx->reconstruct(ei, fi, e, f);

      for (int b = 0; b < batchSize; ++b) {
        for (int i = 0; i < A->shape.dim_at(0); ++i) {
          for (int j = 0; j < B->shape.dim_at(1); ++j) {
            for (int k = 0; k < A->shape.dim_at(1); ++k) {
              tri = mt->triplet_at({i, j, k});
              ee = e[{i, j, k}];
              ff = f[{i, j, k}];
              ci = -1 * identity * ee * ff + ff * A[{i, j}] + ee * B[{k, j}] + tri->z();
              C->setVal({i,j}, ci);
            }
          }
        }
      }

      opCtx->setOutput(0, C);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("MatMulBatch", MatMulBatchKernel);

    ReluKernel::ReluKernel() {
    }

    Status ReluKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MatMulBatchKernel.");

      TensorHandle input = opCtx->getInput(0);
      ShapeHandle shape = input->shape;
      Tensor output;

      GarblerOrEvaluator role = smcCtx->GCRole();

      if (role == kGarbler) {
        if(!smcCtx->garbleACricuit("Relu", shape).ok()) {
          Log(Fatal) << "garbling Relu circuit." << ;
        }
        if(!smcCtx->sendCircuitToEvaluator().ok()) {
          Log(Fatal) << "sending garbled circuit to evaluator.";
        }
        if(!smcCtx->garbleSelfInputAndSend(input).ok()) {
          Log(Fatal) << "garbling self inputs.";
        }
        if(!smcCtx->garblePeerInputAndSendByOT().ok()) {
          Log(Fatal) << "garbling peer inputs by ot.";
        }
        if(!smcCtx->recvGarbledOutput().ok()) {
          Log(Fatal) << "receiving garbled output from peer.";
        }
        if(!smcCtx->degarbleOutput(output).ok()) {
          Log(Fatal) << "degarbling output.";
        }
        if(!smcCtx->sendClearOutputToPeer().ok()) {
          Log(Fatal) << "sending clear output to peer.";
        }
      } else {
        if(!smcCtx->recvGarbledCircuitFromGarbler().ok()) {
          Log(Fatal) << "receiving garbled circuit from gabler.";
        }
        if(!smcCtx->recvGarbledPeerInput().ok()) {
          Log(Fatal) << "receiving garbled peer input.";
        }
        if(!smcCtx->recvSelfGarbledInputByOT(input).ok()) {
          Log(Fatal) << "receiving self garbled input by OT";
        }
        if(!smcCtx->evaluateCircuit().ok()) {
          Log(Fatal) << "evaluating circuit.";
        }
        if(!smcCtx->sendGarbledOutput().ok()) {
          Log(Fatal) << "sending garbled output.";
        }
        if(!smcCtx->recvClearOutputFromPeer(output).ok()) {
          Log(Fatal) << "receving clear output from peer.";
        }
      }

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("Relu", ReluKernel);

    ReluSoftMaxKernel::ReluSoftMaxKernel() {
    }

    Status ReluSoftMaxKernel::Compute(OpContext *opCtx, SMCContext *smcCtx) {
      if(!validateInput(opCtx, ).ok()){
        Log(Fatal) << "Invalid input for ReluSoftMaxKernel.";
      };
      // Compute y_i = \frac{relu(x_i)}{\sum_j relu(x_j)}

      TensorHandle input = opCtx->getInput(0);
      ShapeHandle shape = input->shape;
      Tensor output;

      GarblerOrEvaluator role = smcCtx->GCRole();

      if (role == kGarbler) {
        if(!smcCtx->garbleACricuit("ReluSoftMax", shape).ok()) {
          Log(Fatal) << "garbling ReluSoftMax circuit.";
        }
        if(!smcCtx->sendCircuitToEvaluator().ok()) {
          Log(Fatal) << "sending garbled circuit to evaluator.";
        }
        if(!smcCtx->garbleSelfInputAndSend(input).ok()) {
          Log(Fatal) << "garbling self inputs.";
        }
        if(!smcCtx->garblePeerInputAndSendByOT().ok()) {
          Log(Fatal) << "garbling peer inputs by ot.";
        }
        if(!smcCtx->recvGarbledOutput().ok()) {
          Log(Fatal) << "receiving garbled output from peer.";
        }
        if(!smcCtx->degarbleOutput(output).ok()) {
          Log(Fatal) << "degarbling output.";
        }
        if(!smcCtx->sendClearOutputToPeer().ok()) {
          Log(Fatal) << "sending clear output to peer.";
        }
      } else {
        if(!smcCtx->recvGarbledCircuitFromGarbler().ok()) {
          Log(Fatal) << "receiving garbled circuit from gabler.";
        }
        if(!smcCtx->recvGarbledPeerInput().ok()) {
          Log(Fatal) << "receiving garbled peer input.";
        }
        if(!smcCtx->recvSelfGarbledInputByOT(input).ok()) {
          Log(Fatal) << "receiving self garbled input by OT";
        }
        if(!smcCtx->evaluateCircuit().ok()) {
          Log(Fatal) << "evaluating circuit.";
        }
        if(!smcCtx->sendGarbledOutput().ok()) {
          Log(Fatal) << "sending garbled output.";
        }
        if(!smcCtx->recvClearOutputFromPeer(output).ok()) {
          Log(Fatal) << "receving clear output from peer.";
        }
      }

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("ReluSoftMax", ReluSoftMaxKernel);

    CrossEntropyKernel::CrossEntropyKernel() {}

    Status CrossEntropyKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      if(!validateInput(opCtx, ).ok()){
        Log(Fatal) << "Invalid input for CrossEntropyKernel.";
      };

      // Compute H = -\sum_i S_i log(L_i), where S_i is the true
      // probability and L_i is the predicted probability

      TensorHandle predictedProbs = opCtx->getInput(0);
      TensorHandle trueProbs = opCtx->getInput(1);

      Tensor predictedProbsLog;

      START_A2YY2A_PROTOCOL_FOR_FUNC("Log_N")
        .Role(smcCtx->Role())
        .Input(predictedProbs)
        .Output(predictedProbsLog)
        .Start();

      if (predictedProbsLog->shape != predictedProbs->shape) {
        Log(Fatal) << "Log_N A2YY2A protocol returns tensor with different shape than input.\
                                    Input: " << predictedProbs->shape->debugString() << \
          "Output: " << predictedProbsLog->shape->debugString();
      }

      Tensor entropyProducts;
      Tensor H(0);

      START_ASS_PROTOCOL_FOR_ELEMENTWISE_MULTIPLICATION()
        .Input(PredictedProbsLog)
        .Input(trueProbs)
        .Output(entropyProducts)
        .Start();

      SUM_VECTOR_TO_SCALAR(entropyProducts, H);

      opCtx->setOutput(0, H);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("CrossEntropy", CrossEntropyKernel);

    MaxPoolKernel::MaxPoolKernel() {}

    Status MaxPoolKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MaxPoolKernel.");

      TensorHandle input = opCtx->getInput(0);
      ::std::vector<int> stride = opCtx->getAttr("stride");

      Tensor output;

      START_A2YY2A_PROTOCOL_FOR_FUNC("MaxPool_NHWC_hw")
        .Role(smcCtx->Role())
        .Input(input)
        .Output(output)
        .Attr("N", input->shape->dim_at(0))
        .Attr("H", input->shape->dim_at(1))
        .Attr("W", input->shape->dim_at(2))
        .Attr("C", input->shape->dim_at(3))
        .Attr("h", stride[1])
        .Attr("w", stride[2])
        .Start();

      opCtx->setOutput(0, H);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("MaxPool", MaxPoolKernel);

    ArgMaxKernel::ArgMaxKernel() {}

    Status ArgMaxKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MaxPoolKernel.");

      TensorHandle input = opCtx->getInput(0);

      Tensor output;

      START_A2YY2A_PROTOCOL_FOR_FUNC("ArgMax_N")
        .Role(smcCtx->Role())
        .Input(input)
        .Output(output)
        .Attr("N", input->shape->dim_at(0))
        .Start();

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("ArgMax", ArgMaxKernel);

    ArgMinKernel::ArgMinKernel() {}

    Status ArgMinKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MinPoolKernel.");

      TensorHandle input = opCtx->getInput(0);

      Tensor output;

      START_A2YY2A_PROTOCOL_FOR_FUNC("ArgMin_N")
        .Role(smcCtx->Role())
        .Input(input)
        .Output(output)
        .Attr("N", input->shape->dim_at(0))
        .Start();

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("ArgMin", ArgMinKernel);

    MeanKernel::MeanKernel() {}

    Status MeanKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for MinPoolKernel.");

      TensorHandle input = opCtx->getInput(0);
      int size = input->shape->dim_at(0);

      Tensor output;

      Tensor sum;

      SUM_VECTOR_TO_SCALAR(input, sum);

      START_A2YY2A_PROTOCOL_FOR_FUNC("Division")
        .Role(smcCtx->Role())
        .Input(sum)
        .Input(Tensor(size))
        .Output(output)
        .Start();

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("Mean", MeanKernel);

    ReconstructKernel::ReconstructKernel() {}

    Status ReconstructKernel::Compute(OpContext* opCtx, SMCContext* smcCtx) {
      validateInput(opCtx, "Invalid input for Recon.");

      TensorHandle input = opCtx->getInput(0);
      Tensor output;

      RECONSTRUCT_TENSOR(input, output);

      opCtx->setOutput(0, output);
      opCtx->notify();
      return Status::OK();
    }

    REGISTER_KERNEL_WITH_OP_NAME("Reconstruct", ReconstructKernel);
  }
