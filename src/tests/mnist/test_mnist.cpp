#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>
#include <arpa/inet.h>
#include "../../tensor.h"
#include "../../graph.h"
#include "../../api.h"

#define MNIST_TRAINING_IMAGE_PATH "/home/jason/Downloads/mnist/train-images-idx3-ubyte"
#define MNIST_TRAINING_LABEL_PATH "/home/jason/Downloads/mnist/train-labels-idx1-ubyte"
#define MNIST_TEST_IMAGE_PATH "/home/jason/Downloads/mnist/t10k-images-idx3-ubyte"
#define MNIST_TEST_LABEL_PATH "/home/jason/Downloads/mnist/t10k-labels-idx1-ubyte"

#define M_TRAINING_IMAGE 0
#define M_TRAINING_LABELS 1
#define M_TEST_IMAGES 2
#define M_TEST_LABELS 3

int htohl_fread_item(uint32_t* buf, FILE* f) {
    if (fread(buf, 4, 1, f)==1) {
        *buf = ntohl(*buf);
        return 1;
    } else {
        return 0;
    }
}

int load_images(const char* image_path, uint32_t numimg, uint32_t numrow, uint32_t numcol, secureflow::Tensor* tensor) {
    FILE* f = NULL;
    uint32_t magic_buf;
    uint32_t numimg_buf;
    uint32_t numrow_buf;
    uint32_t numcol_buf;
    f = fopen(image_path, "r");
    htohl_fread_item(&magic_buf, f);
    htohl_fread_item(&numimg_buf, f);
    htohl_fread_item(&numrow_buf, f);
    htohl_fread_item(&numcol_buf, f);
    assert(numimg_buf == numimg);
    assert(numrow_buf == numrow);
    assert(numcol_buf == numcol);
    tensor->create(numimg, numrow, numcol);
    tensor->fill_from_file(f, numimg * numrow * numcol);
    fclose(f);
    return 1;
}

int load_labels(const char* label_path, uint32_t numlbl, secureflow::Tensor* tensor) {
    FILE* f = NULL;
    uint32_t magic_buf;
    uint32_t numlbl_buf;
    f = fopen(label_path, "r");
    htohl_fread_item(&magic_buf, f);
    htohl_fread_item(&numlbl_buf, f);
    assert(magic_buf==2049);
    assert(numlbl_buf==numlbl);
    tensor->create(numlbl);
    tensor->fill_from_file(f, numlbl);
    fclose(f);
    return 1;
}

int test_loading() {
    secureflow::Tensor* tensor_train_image = new secureflow::Tensor;
    secureflow::Tensor* tensor_train_labels = new secureflow::Tensor;
    secureflow::Tensor* tensor_test_image = new secureflow::Tensor;
    secureflow::Tensor* tensor_test_labels = new secureflow::Tensor;

    if (load_images(MNIST_TRAINING_IMAGE_PATH, 60000, 28, 28, tensor_train_image))
        std::cout << "load training images succeeds\n";
    if (load_labels(MNIST_TRAINING_LABEL_PATH, 60000, tensor_train_labels))
        std::cout << "load training labels succeeds\n";
    if (load_images(MNIST_TEST_IMAGE_PATH, 10000, 28, 28, tensor_test_image))
        std::cout << "load test images succeeds\n";
    if (load_labels(MNIST_TEST_LABEL_PATH, 10000, tensor_test_labels))
        std::cout << "load test labels succeeds\n";

    tensor_train_image->cleanup();
    tensor_train_labels->cleanup();
    tensor_test_image->cleanup();
    tensor_test_labels->cleanup();

    return 1;
}

int test_graph() {
    /* Compute y=x^2+2x+1 element-wisely */
    secureflow::Graph* graph = new secureflow::Graph;
    secureflow::Node* src = secureflow::secflow_get_source_node(graph);
    secureflow::Node* sink = secureflow::secflow_get_sink_node(graph);
    int shape[] = {10,10};
    secureflow::Node* x = secureflow::secflow_create_placeholder(graph, shape, 2);
    secureflow::Node* constant_2 = secureflow::secflow_create_constant(graph, 2.f);
    secureflow::Node* constant_1 = secureflow::secflow_create_constant(graph, 1.f);
    secureflow::Node* x_sq = secureflow::secflow_square(x);
    secureflow::Node* x_2 = secureflow::secflow_matmul(constant_2, x);
    secureflow::Node* sum_1 = secureflow::secflow_sum(x_sq, x_2);
    secureflow::Node* sum_2 = secureflow::secflow_sum(sum_1, constant_1);
    secureflow::Session* sess = new secureflow::Session;
    secureflow::Tensor* result = secureflow::secflow_evaluate(sess, graph, sum_2);
    result->print();
}

int main(int argc, char *argv[])
{
    test_graph();
    return 0;
}
