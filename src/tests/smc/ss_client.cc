/*
  secret_sharing.cc -- Compute multiplication using secret sharing
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include "../../smc.h"
#include "../../api.h"


int main(int argc, char *argv[])
{

    int32_t x1 = 15;
    int32_t x2 = 20;
    secureflow::SMCClient c(12345);
    c.wait_servers();
    c.share(x1);
    c.share(x2);
    c.send_triplets(1);

    return 0;
}
