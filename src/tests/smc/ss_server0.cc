#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include "../../smc.h"
#include "../../api.h"


int main(int argc, char* argv[])
{
    secureflow::SMCParty p(12346, "127.0.0.1", 12345);
    int32_t x00;
    int32_t x10;
    int32_t y0;
    int32_t y;
    secureflow::Triplet* tri = NULL;

    p.connect_peer();   printf("Connected to peer.\n");
    p.connect_client(); printf("Connected to client.\n");
    p.recv_share(x00);
    p.recv_share(x10);
    p.recv_triplets(tri);
    secflow_smc_beaver_mul(x00, x10, tri, y0, &p);
    secflow_smc_rec(y0, y, &p);

    printf("x00: %d, x10: %d, y0: %d, y: %d.\n", x00, x10, y0, y);
    printf("bye!");
    return 0;
}
