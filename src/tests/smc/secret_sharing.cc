/*
  secret_sharing.cc -- Compute multiplication using secret sharing
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <inttypes.h>
#include <pthread.h>
#include "../../smc.h"
#include "../../api.h"

typedef struct server_param{
    int32_t x0;
    int32_t x1;
    secureflow::Triplet* tri;
    secureflow::SMCParty* peer;
} server_param_t;

void *server0(void* param) {
    assert(param);
    server_param_t* s_param = (server_param_t *) param;
    int32_t x00;
    int32_t x10;
    secureflow::SMCParty* peer;
    secureflow::Triplet* tri;
    x00 = s_param->x0;
    x10 = s_param->x1;
    peer = s_param->peer;
    tri = s_param->tri;

    int32_t y0;

    peer->connect();
    printf("Connect succeed.");
    secflow_smc_beaver_mul(x00, x10, tri, &y0, peer);

    char pbuf[100];
    sprintf(pbuf, "Share for mul result is %d", y0);
    int ret = write(STDOUT_FILENO, pbuf, strlen(pbuf));
    if (ret < 0) {
        perror("server0 write()");
        exit(1);
    }

    return 0;
}

void *server1(void* param) {
    assert(param);
    server_param_t* s_param = (server_param_t *) param;
    int32_t x01;
    int32_t x11;
    secureflow::SMCParty* peer;
    secureflow::Triplet* tri;
    x01 = s_param->x0;
    x11 = s_param->x1;
    peer = s_param->peer;
    tri = s_param->tri;

    int32_t y1;

    peer->connect();
    printf("Connect succeed.");
    secflow_smc_beaver_mul(x01, x11, tri, &y1, peer);

    char pbuf[100];
    sprintf(pbuf, "Share for mul result is %d", y1);
    int ret = write(STDOUT_FILENO, pbuf, strlen(pbuf));
    if (ret < 0) {
        perror("server1 write()");
        exit(1);
    }

    return 0;
}

void *smc_manager(void* param) {
    int32_t x0;
    int32_t x1;
    x0 = *((int32_t*)param);
    x1 = *(((int32_t*)param)+1);
    int32_t x00 = rand();
    int32_t x10 = x0-x00;
    int32_t x01 = rand();
    int32_t x11 = x1-x10;

    /*
     * Prepare triplets
     */
    secureflow::Triplet* tri = new secureflow::Triplet(1);
    secureflow::Triplet* tri_0 = new secureflow::Triplet(1);
    secureflow::Triplet* tri_1 = new secureflow::Triplet(1);
    tri->init();
    tri->split(tri_0, tri_1);

    secureflow::SMCParty* party0 = new secureflow::SMCParty(23333);
    secureflow::SMCParty* party1 = new secureflow::SMCParty("127.0.0.1", 23333);

    server_param_t param0;
    server_param_t param1;

    param0.x0 = x00;
    param0.x1 = x10;
    param0.peer = party0;
    param0.tri = tri_0;

    param1.x0 = x01;
    param1.x1 = x11;
    param1.peer = party1;
    param1.tri = tri_1;

    pthread_t thr_server0;
    pthread_t thr_server1;
    pthread_create(&thr_server0, NULL, server0, (void*)&param0); // server runs first
    sleep(1); // give server some time to setup socket, and wait for connect request
    pthread_create(&thr_server1, NULL, server1, (void*)&param1);

    int* retval0 = NULL;
    int* retval1 = NULL;
    pthread_join(thr_server0, (void**)&retval0);
    pthread_join(thr_server1, (void**)&retval1);

    if (*retval0 == 0 && *retval1 == 0)
        return 0;
    else
        return (void *)-1;
}

int main(int argc, char *argv[])
{

    int32_t x1 = 100000;
    int32_t x2 = 7777;
    int32_t param[] = {x1, x2};

    return 0;
}
