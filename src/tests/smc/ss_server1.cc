#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include "../../smc.h"
#include "../../api.h"


int main(int argc, char* argv[])
{
    secureflow::SMCParty p("127.0.0.1", 12346, "127.0.0.1", 12345);
    int32_t x01;
    int32_t x11;
    int32_t y1;
    int32_t y;
    secureflow::Triplet* tri = NULL;

    p.connect_peer();   printf("Connected to peer.\n");
    p.connect_client(); printf("Connected to client.\n");
    p.recv_share(x01);
    p.recv_share(x11);
    p.recv_triplets(tri);
    secflow_smc_beaver_mul(x01, x11, tri, y1, &p);
    secflow_smc_rec(y1, y, &p);

    printf("x01: %d, x11: %d, y1: %d, y: %d. \n", x01, x11, y1, y);
    printf("bye!\n");
    return 0;
}
