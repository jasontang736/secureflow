#include <stdlib.h>
#include <stdio.h>
#include "readmnist.h"

const char* mnist_train_label_path = "./data/mnist_train_label";
const char* mnist_train_image_path = "./data/mnist_train_image";
const char* mnist_test_label_path = "./data/mnist_test_label";
const char* mnist_test_image_path = "./data/mnist_test_image";

/*
 * Read the i-th image from the training dataset.
 * Returns the pointer to the first pixel of the image.
 * First, allocate enough byte to contain a 28x28 image
 * Then, read from the file with appropriate offset
 */
byte* secflow_readmnist_data_train_single(int i) {
  byte* data = new byte[28*28];
  FILE* image_file = fopen(mnist_train_image_path, "r");

}
