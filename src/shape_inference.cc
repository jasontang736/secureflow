#include <vector>
#include "shape_inference.h"
#include "node_def.pb.h"
#include "op_def.pb.h"

namespace secureflow {

    namespace shape_inference {

        typedef ::google::protobuf::Map<::std::string, ::secureflow::OpDef_AttrDef> Attr;

        Shape::Shape() : rank_(0) {}
        Shape::Shape(const ::std::vector<int>& dims) : dims_(dims), rank_(dims.size()) {}
        int Shape::dim_at(int d) {
            SF_ASSERT(dims_) << "dims_ is NULL, may have been scalarized.";
            return dims_[d];
        }

        InferenceContext::InferenceContext(
                         const NodeDef* node_def, const OpDef& op_def,
                         const std::vector<TensorShapeProto>& input_shapes,
                         const std::vector<const Tensor*>& input_tensors,
                         const std::vector<TensorShapeProto>& input_tensors_as_shapes,
                         const std::vector<
                         std::unique_ptr<std::vector<std::pair<TensorShapeProto, DataType>>>>&
                         input_handle_shapes_and_types) :
            node_def_(SF_CHECK_NOTNULL(node_def))
        {
            node_def_ = node_def;
            op_def_ = op_def_;

        }

        InferenceContext::Run(const std::functon<Status(shape_inference::InferenceContext)>& fn) {
            Status s = fn(this);
            SF_REQUIRES_OK(s) << "Failed to run ShapeInferenceContextFn.";
            return s;
        }

        Status InferenceContext::set_output(int idx, ShapeHandle output_shape_handle) {
            if (output_handles_[idx].IsSet()) {
                return errors::InvalidArgument(idx, "-th output shape is already set.");
            }
            output_handles_[idx] = output_shape_handle;
            return Status::OK();
        }

        Status InferenceContext::subShape(ShapeHandle inShape, int startDim, int endDim, ShapeHandle outShape) {
            // Append the dimension of inShape from startDim to endDim
            // to outShape. (Endpoints inclusive)
            for (int d = startDim; d <= endDim; ++d) {
                outShape->addDimension(inShape->dim_at(d));
            }
            return Status::OK();
        }

        Status Flatten(ShapeHandle inShape, ShapeHandle outShape) {
            int inRank = inShape->rank();
            if (inRank == 0) {
                return errors::InvalidArgument("inRank in Flatten() should not be 0.");
            }
            if (inRank == 1) {
                return errors::InvalidArgument("inRank is 1, there is no need for Flatten().");
            }
            int outDim = 1;
            for (int d = 0; d < inRank; ++d) {
                outDim *= inShape->dim_at(d);
            }
            outShape->addDimension(outDim);
            return Status::OK();
        }

        template <>
        Status getAttr(StringPiece attr_name, StringPiece* value) {
            ::secureflow::OpDef_AttrDef val = op_def_.attr()[attr_name.as_string()];

            if (val.type() != "string"){
                return error::InvalidArgument(attr_name,
                    " refers to a non-string attribute (by checking type value)");
            }
            if (!val.has_s()) {
                return error::InvalidArgument(attr_name,
                    " refers to a non-string attribute (by checking has_s())");
            }
            (*value) = *val.release_s();
            return Status::OK();
        }

        template <>
        Status getAttr(StringPiece attr_name, ::std::vector<int>* value) {
            ::secureflow::OpDef_AttrDef val = op_def_.attr()[attr_name.as_string()];

            if (val.type() != "list(int)") {
                return error::InvalidArgument(attr_name,
                    " refers to a non-list(int) attribute (by checking type value)");
            }
            if (!val.has_list()) {
                return error::InvalidArgument(attr_name,
                    " refers to a non-list(int) attribute (by checking has_list())");
            }
            list = *val.release_list();

            const ::google::protobuf::RepeatedField< ::google::protobuf::int64> list;
            list = val.release_list()->i();

            for(int j = list.begin(); j != list.end(); j++) {
                value->push_back(j);
            }
        }

        Status ArgOpShape(ShapeInferenceContext *ctx) {
            // Compute the shape of an Arg operation
            // E.g.
            // X: 10x20, dimension: 0 =====> out: 20
            // X: 10x20x30, dimension: 0 =====> out: 20x30
            // X: 10x20x30, dimension: 1 =====> out: 10x30
            // X: 10x20x30, dimension: 2 =====> out: 10x20

            ShapeHandle* in = ctx->get_input(0);
            int rank = in->rank();
            int dimension;
            SF_REQUIRES_OK(ctx->getAttr("dimension", &dimension))\
                << "Unable to get dimension";
            // Verify that dimension is less than the in->rank()
            SF_ASSERT(dimension <= rank)\
                << "Dimension exceeds tensor rank";
            ShapeHandle* out = new ShapeHandle();

            if (rank = 1) {
                // Scalar shape
                out->asScalar();
            } else {
                if (dimension == 0) {
                    ctx->subShape(in, 1, rank, out);
                } else if (dimension == rank) {
                    ctx->subShape(in, 0, rank-1, out);
                } else {
                    ctx->subShape(in, 0, dimension-1, out);
                    ctx->subShape(in, dimension+1, rank, out);
                }
            }

            ctx->set_output(0, out);
            return Status::OK();
        }

        Status UnchangedShape(ShapeInferenceContext* ctx) {
            ShapeHandle* in = ctx->get_input(0);
            ctx->set_output(0, in);
            Return Status::OK();
        }

        Status ReductionShape(ShapeInferenceContext* ctx) {
            // Compute shape for ops such as mean
            // E.g.
            // X: 10x20x30, reduction_indices: (1) ====> z: 10x30

            ShapeHandle* in = ctx->get_input(0);
            int rank = in->rank();
            int reductionIndex;
            SF_REQUIRES_OK(ctx->getAttr("reductionIndex", &reductionIndex))\
                << "Unable to get reduction index";
            // Verify that dimension is less than the in->rank()
            SF_ASSERT(dimension <= rank)\
                << "Reduction index exceeds tensor rank";
            ShapeHandle* out = new ShapeHandle();

            if (rank = 1) {
                // Scalar shape
                out->asScalar();
            } else {
                if (reductionIndex == 0) {
                    ctx->subShape(in, 1, rank, out);
                } else if (reductionIndex == rank) {
                    ctx->subShape(in, 0, rank-1, out);
                } else {
                    ctx->subShape(in, 0, reductionIndex-1, out);
                    ctx->subShape(in, reductionIndex+1, rank, out);
                }
            }

            ctx->set_output(0, out);
            return Status::OK();
        }
    }
}
