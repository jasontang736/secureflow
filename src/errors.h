#ifndef SECUREFLOW_ERRORS_H_
#define SECUREFLOW_ERRORS_H_

#define SF_RETURN_IF_ERROR(...)                                         \
    const ::secureflow::Status _status = (__VA_ARGS__);                 \
    if (!_status.ok()) return _status;

#define SF_RETURN_WITH_CONTEXT_IF_ERROR(expr, ...)                      \
    ::secureflow::Status _status = (expr);                              \
    if (!_status.ok()) {                                                \
        ::secureflow::errors::AppendToMessage(&_status, __VA_ARGS__);   \
        return _status;                                                 \
    }                                                                   \

#define SF_REQUIRES_NOT_NULL(expr, ...)         \
  if (!expr) {                                  \
    LOG(FATAL) << __VA_ARGS__;                  \
  }                                             \


#endif
