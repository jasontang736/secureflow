#ifndef SECUREFLOW_SMC_CONTEXT_H_
#define SECUREFLOW_SMC_CONTEXT_H_

#include <arpa/inet.h>
#include <string>
#include "status.h"
#include "tensor.h"

namespace secureflow {

     class Triplet;
     class SMCAddress;
     class NetworkHelper;

     typedef unsigned char byte;

     class SMCContext {
     private:
          SMCAddress* thisAddr;
          SMCAddress* otherAddr;
          NetworkHelper* networkHelper;
     public:
          Status validateTriplet(Triplet&);
          Status reconstruct(Tensor& ei, Tensor& fi, Tensor& e, Tensor& f);
     };

     class SMCAddress {
     private:
          in_addr_t addr_;
     public:
          SMCAddress(std::string ip, int port);
     };

     class NetworkHelper {
     private:
          SMCAddress peer_address_;
     public:
          Status send(byte* data, int length);
          Status recvLength(int * length);
          Status recv(byte* buf, int length);
     };
}

#endif
