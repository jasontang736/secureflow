#ifndef SECUREFLOW_GRAPH_H_
#define SECUREFLOW_GRAPH_H_

#include <string>
#include <vector>
#include <memory>
// #include "types.h"
#include "errors.h"
#include "macros.h"
#include "logging.h"
#include "op.h"
#include "variable.h"

namespace secureflow {

  class Node;
  class Edge;
  class Graph;
  class NeighborIter;
  typedef std::vector<Edge*> EdgeSet;

  class OpRegistry;
  class Variable;
  class VariableStore;

  class Node {
  private:
    enum NodeType {
      NT_Source,
      NT_Sink,
      NT_Variable,
      NT_Constant,
      NT_Placeholder,
      NT_Op,
    };
    NodeType nodeType_;
    std::string name_;
    std::string op_;
    int64 id_;
    int dirty_;
    std::vector<Edge*> in_edges_; // does not own the actual edges
    std::vector<Edge*> out_edges_; // does not own the actual edges
    Node* grad_node_;
    Graph* graph_;   // backward reference

    friend class NeighborIter;
    friend class Graph;
  public:
    Node(std::string name) : name_(name), dirty_(0) {}
    ~Node();

    inline int dirty() { return dirty_; }

    bool is_op() { return nodeType_ == NT_Op; }

    bool is_variable() { return nodeType_ == NT_Variable; }

    const Edge* in_edge(int idx) { return in_edges_[idx]; }

    const Edge* out_edge(int idx) { return out_edges_[idx]; }

    EdgeSet* in_edges() { return &in_edges_; }

    EdgeSet* out_edges() { return &out_edges_; }

    int id() { return id_; }

    std::string name() { return name_; }

    std::string op() { return op_; }

    void set_grad_node(Node* grad_node);

    Node* grad_node() { return grad_node_; }

    Tensor* value();

    inline Graph* graph() { return graph_; }

    inline std::vector<Node*>& in_neighbors();
    inline std::vector<Node*>& out_neighbors();
    int in_neighbor_size();
    int out_neighbor_size();
  };


  class Edge {
  private:
    Node* src_;
    Node* dst_;
    int src_output_;
    int dst_input_;
  public:
    Edge(Node* src, Node* dst, int src_output, int dst_input)
      : src_(src), dst_(dst), src_output_(src_output), dst_input_(dst_input) {}
    ~Edge();
    Node* src() const  { return src_; }
    Node* dst() const { return dst_; }
    int src_output() { return src_output_; }
    int dst_input() { return dst_input_; }
  };

  // Thread-unsafe
  class NeighborIter {
  private:
    Node* n_;
    bool incoming_;
    int idx_;
  public:
    NeighborIter(){}
    NeighborIter(Node* n, bool incoming)
      : n_(n), incoming_(incoming), idx_(0) {}
    Node* operator*() {
      return incoming_ ? n_->in_edge(idx_)->src() : n_->out_edge(idx_)->dst();
    };
    NeighborIter & operator+(int n) { idx_+=n; return *this; }
    NeighborIter & operator++() { idx_++; return *this; }
    NeighborIter & operator-(int n) { idx_-=n; return *this; }
    NeighborIter & operator--() { idx_--; return *this; }
    bool operator==(NeighborIter & rhs) {
      return (n_ == rhs.n_) &&
        (incoming_ == rhs.incoming_) &&
        (idx_ == rhs.idx_);
    }
    int idx() { return idx_; }
  };

  class Graph {
  private:
    OpRegistry* op_registry_;
    Node* source_;
    Node* sink_;
    std::vector<Node*> nodes_;
    std::vector<Edge*> edges_;
    int id_counter_;
  public:
    Graph();
    Graph(OpRegistry & op_registry);
    ~Graph();

    Node* source() { return source_; }

    Node* sink() { return sink_; }

    Node* create_placeholder(Shape* shape);

    Node* create_constant(float value);

    Node* create_constant(Tensor* t);

    Node* applyOp(Node* n, std::string OpName);

    Node* applyOp(Node* a, Node* b, std::string OpName);

    Node* getNodeById(int id);

    Edge* addEdge(Node* src, Node* dst, int src_output, int dst_input);

    Edge* addEdge(Node* src, Node* dst, int src_output);

    void newNameId(std::string & name, int64 & id);

    Op* getOpByName(string op_name);
  };

}

#endif //SECUREFLOW_GRAPH_H_
