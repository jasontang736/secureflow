#ifndef SECUREFLOW_API_VARIABLE_H_
#define SECUREFLOW_API_VARIABLE_H_

#include "../tensor.h"
#include "../shape.h"
#include "../errors.h"
#include "../macros.h"
#include <string>

namespace secureflow_api {
    class Variable {
    private:
        Tensor tensor_; // owns
        Shape shape_; // owns
        std::string name_;

        NodeOutput bindedNodeOutput_;

    public:
        SF_DISALLOW_COPY_AND_ASSIGN(Variable);
    }

}

#endif
