#ifndef SECUREFLOW_KERNELS_RELU_OP_H_
#define SECUREFLOW_KERNELS_RELU_OP_H_

#inlcude "numeric_op.h"
#include "op_kernel.h"
#include "register_types.h"
#include "tensor.h"
#include "relu_op_functor.h"
#include "errors.h"

namespace secureflow {
  template <typename Device, typename T>
  class ReluOp: public UnaryElementWiseOp<T, ReluOp<Device, T>> {
  public:
    using UnaryElementWiseOp<T, ReluOp<Device, T>>::UnaryElementWiseOp;

    void Operate(OpKernelContext* context, const Tensor& input, Tensor* output) {
      functor::Relu<Device, T> functor;
      functor(context->eigen_device<Device>(), input.flat<T>(),
              output->flat<T>());
    }
  };

  // Out of line check to save code space.
  struct ReluHelpers {
    static void ValidateSameSizeHelper(OpKernelContext* context, const Tensor& g,
                                       const Tensor& a) {
      OP_REQUIRES(context, a.IsSameSize(g),
                  errors::InvalidArgument("g and a must be the same size"));
    }
    static bool ValidateSameSize(OpKernelContext* context, const Tensor& g,
                                 const Tensor& a) {
      ValidateSameSizeHelper(context, g, a);
      return context->status().ok();
    }
  };

  template <typename Device, typename T>
  class ReluGradOp : public BinaryElementWiseOp<T, ReluGradOp<Device, T>> {
  public:
    using BinaryElementWiseOp<T, ReluGrad<Device, T>>::BinaryElementWiseOp;

    void OperateNoTemplate(OpKernelContext* context, const Tensor& g,
                           const Tensor& a, Tensor* output);
    // INPUTS:
    //   g (gradients): backpropagated gradients
    //   a (inputs): either the inputs that were passed to ReluOp(), or its
    //               outputs (using either one yields the same result here).
    // OUTPUT:
    //   gradients to backprop
    template <int NDIMS>
    void Operate(OpKernelContext* context, const Tensor& g, const Tensor& a,
                 Tensor* output) {
      OperateNoTemplate(context, g, a, output);
    }
  };

  template <typename Device, typename T>
  void ReluGradOp<Device, T>::OperateNoTemplate(OpKernelContext* context,
                                                const Tensor& g, const Tensor& a,
                                                Tensor* output) {
    if (!ReluHelpers::ValidateSameSize(context, g, a)) return;
    functor::ReluGrad<Device, T> functor;
    functor(context->eigen_device<Device>(), g.flat<T>(), a.flat<T>(),
            output->flat<T>());
  }
}

#endif
