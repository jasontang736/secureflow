#include "thread_pool.h"

namespace secureflow {

    ThreadPool::ThreadPool(size_t n) {
        {
            Log(INFO) << "Initializing ThreadPool of size " << n << "\n";
        }
        for (int i = 0; i < n; ++i) {
            workers_.emplace_back([this](){
                    while(true) {
                        std::function<void()> bindedReadyKernel;

                        {
                            std::unique_lock<std::mutex> lock(this->queue_mutex_);

                            this->condition_.wait(lock,
                                                  [this](){return this->stop\
                                                          || !this->bindedReadyKernel_.empty(); });
                            if (this->stop && this->bindedReadyKernel_.empty())
                                return;

                            bindedReadyKernel = std::move(this->bindedReadyKernel_.front());
                            this->bindedReadyKernel_.pop_front();
                        }

                        bindedReadyKernel();
                    }
                });
        }
    }

    template <typename KernelType>
    auto ThreadPool::enqueue(KernelType* k, OpContext* opCtx, SMCContex* smcCtx) {

        auto bindedKernelTask = std::make_shared< std::packaged_task<Status()> >(
                        std::bind(std::forward<KernelType*>(k), std::forward<OpContext*>(opCtx),
                                  std::forward<SMCContext*>(smcCtx)));

        std::future<Status> res = bindedKernelTask->get_future();
        {
            std::unique_lock<std::mutex> lock(queue_mutex_);

            if (stop)
                throw std::runtime_error("enqueue on stopped ThreadPool.");

            bindedReadyKernels_.emplace_back([bindedKernelTask](){
                    (*bindedKernelTask)();
                });
        }
        condition_.notify_one();
        return res;
    }

    inline ThreadPool::~ThreadPool() {
        {
            std::unique_lock<std::mutex> lock(queue_mutex_);
            stop = true;
        }
        condition_.notify_all();
        for (std::thread &worker : workers_) {
            worker.join();
        }
    }
}
