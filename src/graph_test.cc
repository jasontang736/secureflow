#include "graph.h"
#include "gtest/gtest.h"

// Fixture
class GraphTest : public ::testing::Test {
public:
  Graph* graph_; // owned

  void SetUp() {
    OpRegistry* ops = OpRegistry::Gloabl();
    graph_ = new Graph(ops);
  }

  void TearDown() {
    delete graph_;
  }
}

// test source and sink are instantiated
  TEST_F(GraphTest, Constructor) {
    Node* source = graph_.source_node();
    EXPECT_NE(source, nullptr);
    Node* sink = graph_.sink_node();
    EXPECT_NE(sink, nullptr);
  }

// test the node counting works
TEST_F(GraphTest, AddNode) {
  NodeDef def;
  def.set_name(name);
  def.set_op("NoOp");

  int num_nodes = 100;
  Status status;
  for(int i = 0; i < num_nodes; i++) {
    AddNode(def, &status);
    TF_CHECK_OK(status);
  }
  EXPECT_EQ(graph_.num_op_nodes, 100)
    }
