* Todo
** DONE Implement AddN functor
   CLOSED: [2017-11-30 Thu 10:09]
* Design choices
** Design of MT generation
Q1: What is the protocol for pre-computing multiplication triplet?

  Option 1

    Before protocol execution, the executor traverse the entire graph
    and count the number of multiplications we need. Then for each
    multiplication we need, we generate a multiplication triplet

      <u> * <v> = <z>

    , using Paillier encryption.

    The benefit is that the online phase is much faster. The downside
    is that we need to store all the triplets, either in memory or
    disk, which results in heavy memory footprint.

  Option 2

    We don't generate prior to protocol execution, but do so during
    execution on the fly.

    The benefit is that no triplets other than those being needed are
    stored in memory, thereby minimizing memory footprint.


Option 1 is better, since we can store most triplet on disk, and
maintain only a fixed-size triplet storage in memory, therefore
minimizing memory footprint. Here the size of the storage will be a
hyper parameter.
*
** Design of tensors Q1 Should we initialize all tensors all at once? Yes, and this means we should not allocate space for tensors during execution, we only need to modify them.
** Design of MatMulKernel
Q How to ensure both parties are using the same triplet?

  Option 1
    We give every triplet a unique id, and associate that id with each
    op node (which requires multiplication) in the graph. Thus, by the
    time the op node is about to execute, it can use that id to fetch
    the correct triplet from the triplet pool.

    The benefit is a simple implementation. The downside is that it
    requires additional memory for storing each multiplication id.

  Option 2
    We require that two parties are executing the same node(s) at a
    time. And each op node is given an id only when it's in the
    execution pool. Suppose there is a execution pool of size 10, and
    one of the op is MatMul, we then assign this node an id, say, 7.
    Now the MatMul in both parties have the same id, they can make a
    query.

    Downside, this does not guarantee synchronization.

  Option 3
    We build a notification system, such that after a node completes
    execution, it sends notification of completion not only to its
    child nodes, but also the counterpart in the other party.
    Therefore, when one node is asking for triplet, its counterpart in
    the other party must also be in execution state. In such case, we
    can use a finite number of ids to label the executing node, and
    they will get the correct triplet.

    Benefit: this enforces synchronization. Downside: this requires
    one communication per node. However, since the number of node is
    much smaller than the number of multiplication, it is manageable.

Q How to perform tensor multiplication?

  What does it mean to multiply A: a1 x a2 x a3 x...x an shape, with
  B: b1 x b2 ...

  We don't need tensor multiplication (which is formally called tensor
  contraction). We only need matrix multiplication.

** How to move forward faster.

  I realized that my problem with developing is that I strive for
perfection from day one, which is impossible and unecessary in the
first place. From now on, I'll write dirty code.

** Cross Entropy, which first?
   In information theory, the cross entropy between two probability
   distribution p and q over the same underlying set of events
   measures the average number of bits required to identify an event
   drawn from the set, if a coding scheme is used that is optimized
   for an "unnatural" probability distribution q, rather than the
   "true" distribution p.

** Minimization process
When user calls =train_step.eval()=, what happens?
Option 1

  For each Step, the GraphManager object performs the following tasks:

  We firstly build a DAG of forward phase (through the Python calls),
and evaluate it (don't stop until we reach the loss node). Then we put
this DAG into sleep.

  Then we build a DAG (if not existed, else wake it
up) of backward phase by reversely iterating each node from end to
start. At the meantime, we attach output tensors of the forward graph
to the input edge of the backward graph, and a new (if not existed)
tensors (the partial derivative) of each of the forward graph's input
edge to the output edge. We should pay attention to the order of
inputs, since the backward kernel of the node is specified with order
considered. We then evaluate this backward DAG. Note that we fully
evaluate each output (the partial derivative) of each backward node,
as long as the output contains a Variable tensor. If the output edge
contains constant tensor (placeholders will become constant, if not
specified), we omit the computation. After we obtain the partial
derivatives of each Variable, we put this DAG into sleep.

  Then we build another DAG (if not existed, else wake it up) of apply
phase, by iterating all Variables and create a minus (if
minimization is called, else plus) node for each Variable. This node
receives the Variable, the partial derivatives and a constant value
(learning rate) as inputs, and produces no outputs. The Compute()
step of these nodes will only notify the GraphManager. And these nodes
are the only nodes that can alter the value of input tensors. Other nodes
only alter the value of output tensors.

    However, for the consistency of the Compute() interface, we can
    alternatively assign the input node as the output node, thereby
    achieving the 'apply' functionality.

** What is executor's job? And its functionality?
The executor holds a threadpool, and a queue of kernel's computation
jobs. Upon start it uses a while loop to add all nodes to the FIFO
queue. As long as the threadpool has an available thread, the queue
will pops its front element, checks if all of its input ports have
been notified. If so, this node is assigned to the available thread
and immediately executed. If not, we put this node into a temporary
FILO queue, and pop the previous queue for another node and do the
checks iteratively. If the previous queue is empty and no node is
eligible for execution, report an error (this graph may be
disconnected somewhere). After a node is found to be executable, we
pops node from the temporary node one by one and puts in the front of
the previous queue.

** How do we make sure that two executors are synced?
Remote notification combined with local notification.


** How does notification work?
Each node, upon finish, sends a notification to all of its dependent
node. Each node, upon receiving notification, request the graph
manager to add itself to the graph manager's threadpool
* Questions
** What is the difference between Op and Kernel?
Op is device independent while kernel is device dependent.
One Op may correspond to many kernels. For example, the multiplication Op may
have different kernels, one for cpu-only kernel, one for gpu-only kernel.
** Why do we need to register Op?
There is a global Op Registry for the library. During compile time, we specify
the Op and its corresponding kernels, and basically insert it to the registry.

During graph building time, C APIs are called with Op name, shape information.
The global OpRegistry helps decide whether the requested Op is available, and
whether the shape information given is valid. If the op is available and the
shape information is valid, it runs the shapeinference function stored in the
registration of the Op, and deduce the output shape function. And then it stores
the output shape to the op. The op is copied and a pointer of it is stored in
the created Node.
** What kind of shape information do we need?
1. The number of inputs (each input corresponding to a port, which then
   corresponds to a parent node's op), the dimension of each input, and the type
   of each input.
2. The dimension of each input
3. The type of each input

To give an example, we may have things like:
  Input("N1 : T1")
  Input("T2")

We call strings like "N1 : T1" and "T2" the "input shape spec"

That's not all the shape information, that's more sort of "input information".
The semantic is that the input shape spec(spec) can only contain at most 2
names, which are separated using the colon. If the input shape spec contains two
names, then we require the first name ("N1" in this example) to represent the
"port number", and the second name ("T1" in this example) to represent the
"typename". If the spec only contains 1 name, then that name represent the
typename. Meanwhile, the number of input port gets increased by calling
`Input()`. If there are two names (two name spec), then the number of increased
input ports is the value represented by "N1". If it's one name spec, then the
number of increased ports is simply 1.

After this Op is instantiated and assigned to a node, by calling the C-APIs to
construct the graph. The value represented by "N1" is automatically decided. For
example, if the node turns out to have 3 input nodes, then "N1" is decided to
be 2.

*** How to implement that?

**** What are the C-APIs?
void secflow_init_graph(Graph* g);
Node* secflow_get_source_node(Graph* g);
Node* secflow_get_sink_node(Graph* g);
Node* secflow_create_node(Graph* g, string op_name);
Node* secflow_create_placeholder(Graph* g, int[] shape);
Node* secflow_create_variable(Graph* g, int[] shape);
Node* secflow_matmul(Node*, Node*); -> will report error if shape is invalid.
Node* secflow_sigmoid(Node*);
Node* secflow_tanh(Node*);
Node* secflow_sum(Node* ...);
Node* secflow_backpropagte(Node* y, Node* hy); -> return an "Apply" Node
Node* secflow_argmax(Node*);
Node* secflow_rungraph(Graph* g);
** How does the node knows which input it connects to?
Since each node basically can have infinite number of inputs, it's crucial to
have a mechanism for the programmers to know which input it connects to. This is
required so that the shape inference function works as desired.

Here's the deal:
  During Op registration time, we call something like Input("...").Input("...").
  These function call is actually ordered.
* Tasks
** TODO summarize important posix header
  [2018-02-23 Fri]
  [[file:~/sandbox/secureflow/src/smc.h::#include%20<inttypes.h>]]
** TODO Summarize POSIX headers
  [2018-02-23 Fri]
  [[file:~/sandbox/secureflow/src/smc.h][file:~/sandbox/secureflow/src/smc.h]]
