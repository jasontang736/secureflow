#include <memory>
#include <algorithm>
#include <iterator>
#include "tensor.h"
// #include "tensor.pb.h"
#include <assert.h>
#include <iostream>

namespace secureflow {


  bool Shape::operator==(const Shape& rhs) const {
    if (rhs.rank() != rank()) return false;
    for (uint32_t i = 0 ; i < rank() ; ++i) {
      if (rhs.dim_at(i) != dim_at(i)) return false;
    }
    return true;
  }


  Tensor::Tensor() {
    data_ = NULL;
    shape_ = new Shape();
    type_ = f32;
    dirty_ = false;
  }

  /*
   * NOTE: These create function will replace the old data if any
   */

  void Tensor::create(uint32_t dimA) {
    if (dirty_)
      cleanup();
    data_ = new byte[dimA * sizeof(uint32_t)];
    shape_->add_dim(dimA);
    dirty_ = true;
  }

  void Tensor::create(uint32_t dimA, uint32_t dimB) {
    if (dirty_)
      cleanup();
    data_ = new byte[dimA * sizeof(uint32_t)];
    shape_->add_dim(dimA);
    shape_->add_dim(dimB);
    dirty_ = true;
  }

  void Tensor::create(uint32_t dimA, uint32_t dimB, uint32_t dimC) {
    if (dirty_)
      cleanup();
    data_ = new byte[dimA * dimB * dimC * sizeof(uint32_t)];
    shape_->add_dim(dimA);
    shape_->add_dim(dimB);
    shape_->add_dim(dimC);
    dirty_ = true;
  }


  /*
   * Create a tensor with the same shape as s.
   */
  void Tensor::create(Shape* s, ValueType type) {
    uint32_t size = type == f32 ? sizeof(float) : ( (type == u32) ? sizeof(uint32_t) : sizeof(uint8_t));
    for (uint32_t i = 0; i < s->rank(); ++i) {
      size *= s->dim_at(i);
      shape_->add_dim(s->dim_at(i));
    }
    data_ = new byte[size];
  }

  /*
   * Aggregate value from the rhs tensor.
   */
  int Tensor::aggregateFrom(Tensor* rhs) {
    assert(*shape_ == *rhs->shape());
    for (uint32_t i = 0; i < shape_->rank(); ++i) {

    }

    return 0;
  }

  /*
   * Fill tensor with an integer
   */
  void Tensor::fill(int32_t bits) {
    assert(data_);
    memcpy(data_, &bits, sizeof(bits));
  }

  /*
   * Fill tensor with integers
   */
  void Tensor::fill(int32_t* bits, uint32_t size) {
    assert(data_ && data_ + (size - 1));
    memcpy(data_, bits, sizeof(int32_t) * size);
  }

  /*
   * Fill tensor with bytes
   */
  void Tensor::fill(byte* data, uint32_t size) {
    assert(data_ && data_ + (size - 1));
    memcpy(data_, data, size);
  }

  /*
   * Fill tensor with data in `f`, according to its current shape
   */
  void Tensor::fill_from_file(FILE* f, uint64_t size) {
    assert(shape_->size() == size);
    assert(fread(data_, 1, size, f) == size);
    printf("Not implemented!");
    abort();
  }

  /*
   * Reinterpret the tensor with new value type.
   * Must be called after data is filled.
   */
  void Tensor::set_type(ValueType t) {
    if (t == f32 || t == u32) {
      /* Test whether size is a multiple of 32 */
      assert(shape_->size() > 0);
      assert(shape_->size() % 32 == 0);
    }
    type_ = t;
  }

  /*
   * Get value from tensor
   */
  void Tensor::get(uint32_t pos, byte** b) {
    if (type_ == f32) {
      float* fb = (float*)*b;
      *fb = *((float*)(data_ + pos));
    } else if (type_ == u32) {
      uint32_t* ub = (uint32_t*)*b;
      *ub = *((uint32_t*)(data_ + pos));
    } else if (type_ == i32) {
      int32_t* ib = (int32_t*)*b;
      *ib = *((int32_t*)(data_ + pos));
    } else if (type_ == u8) {
      uint8_t* u8b = (uint8_t*)*b;
      *u8b = *((uint8_t*)(data_ + pos));
    }
  }

  /*
   * Set value for cell in tensor.
   */
  int Tensor::set(CellIndex& idx, uint32_t size, byte* data) {
    // TODO: implement it
  }

  /*
   * Cleanup
   */
  void Tensor::cleanup() {
    if (dirty_)
      delete [] data_;
    dirty_ = false;
    shape_->cleanup();
  }

  /*
   * Print values as a one-dimensional array
   */
  void Tensor::print() {
    int elemsize = 0;
    byte* current_byte = this->data_;
    float fval;
    uint32_t u32val;
    uint8_t u8val;
    uint32_t size = this->shape_->size();
    if (this->type_ == f32 || this->type_ == u32)
      elemsize = 32;
    else
      elemsize = 8;

    std::cout << "[";
    for (uint32_t i = 0; i < size; ++i) {
      current_byte += i * elemsize;
      if (this->type_ == f32) {
        fval = *((float *)current_byte);
        std::cout << fval;
      } else if (this->type_ == u32) {
        u32val = *((uint32_t *)current_byte);
        std::cout << u32val;
      } else if (this->type_ == u8) {
        u8val = *((uint32_t *)current_byte);
        std::cout << u8val;
      }
      if (i < size - 1)
        std::cout << ", ";
    }
    std::cout << "]\n";
  }

  /*
   * Create triplet (but not fill tensors with value)
   *
   */

  Triplet::Triplet(uint32_t n) {
    tu_ = new Tensor;
    tv_ = new Tensor;
    tz_ = new Tensor;
    tu_->create(n);
    tv_->create(n);
    tz_->create(n);
    size_ = n;
  }

  /*
   * Initialize triplet with random values.
   * Note that the rand() call is not cryptographic secure!
   */
  int Triplet::init() {
    uint32_t n = size();
    int32_t us[n];
    int32_t vs[n];
    int32_t zs[n];
    for (uint32_t i = 0; i < n; ++i) {
      us[i] = rand() % (1<<8); // make sure they are at most 16 bit long
      vs[i] = rand() % (1<<8);
      zs[i] = us[i] * vs[i];
    }
    tu_->fill(us, n);
    tv_->fill(vs, n);
    tz_->fill(zs, n);
    return 0;
  }

  /*
   * Split tu_, tv_, tz_ into tu0, tu1, tv0, tv1, tz0, tz1
   */
  int Triplet::split(Triplet* tri0, Triplet* tri1) {
    assert(tri0);
    assert(tri1);
    assert(tri0->size() == size());
    assert(tri1->size() == size());

    uint32_t n = size();
    int32_t u0s[n];
    int32_t u1s[n];
    int32_t v0s[n];
    int32_t v1s[n];
    int32_t z0s[n];
    int32_t z1s[n];

    int32_t tmp = 0;
    int32_t* tmp_p = &tmp;
    for (uint32_t i = 0; i < n; ++i) {
      u0s[i] = rand() % ( 1 << 8 );
      tu_->get(i, (byte**)&tmp_p);
      u1s[i] = tmp - u0s[i];

      v0s[i] = rand() % (1 << 8 );
      tv_->get(i, (byte**)&tmp_p);
      v1s[i] = tmp - v0s[i];

      z0s[i] = rand() % (1 << 8 );
      tz_->get(i, (byte**)&tmp_p);
      z1s[i] = tmp - z0s[i];
    }

    tri0->fill(u0s, v0s, z0s);
    tri1->fill(u1s, v1s, z1s);

    return 0;
  }

  /*
   * Fill the triplet. Please provide data with consistent size!
   */
  int Triplet::fill(int32_t* us, int32_t* vs, int32_t* zs) {
    assert(us);
    assert(vs);
    assert(zs);
    tu_->fill(us, size());
    tv_->fill(vs, size());
    tz_->fill(zs, size());

    return 0;
  }

  /*
   * Get a single `u`
   */
  void Triplet::getu(uint32_t pos, int32_t& u) {
    int32_t* u_p = &u;
    tu_->get(pos, (byte**)&u_p);
  }

  /*
   * Get a single `u`
   */
  void Triplet::getv(uint32_t pos, int32_t& v) {
    int32_t* v_p = &v;
    tv_->get(pos, (byte**)&v_p);
  }

  /*
   * Get a single `u`
   */
  void Triplet::getz(uint32_t pos, int32_t& z) {
    int32_t* z_p = &z;
    tz_->get(pos, (byte**)&z_p);
  }

  /*
   * Fill triplet with bytes
   */
  int Triplet::fill_with_bytes(byte* u, byte* v, byte* z, uint32_t size) {
    assert(u); assert(v); assert(z);

    // delte old tensor if present
    // TODO: consider the case where the old size equals new size
    if (tu_)
      tu_->cleanup();
    else
      tu_ = new Tensor();

    if (tv_)
      tv_->cleanup();
    else
      tv_ = new Tensor();

    if (tz_)
      tz_->cleanup();
    else
      tz_ = new Tensor();

    tu_->create(size);
    tu_->fill(u, size);

    tv_->create(size);
    tv_->fill(v, size);

    tz_->create(size);
    tz_->fill(z, size);

    return 0;
  }

  /*
   * Get the byte for u tensor
   */
  byte* Triplet::getubyte() {
    assert(tu_); assert(tu_->data_);
    return tu_->data_;
  }

  /*
   * Get the byte for v tensor
   */
  byte* Triplet::getvbyte() {
    assert(tu_); assert(tu_->data_);
    return tv_->data_;
  }

  /*
   * Get the byte for z tensor
   */
  byte* Triplet::getzbyte() {
    assert(tu_); assert(tu_->data_);
    return tz_->data_;
  }
}
