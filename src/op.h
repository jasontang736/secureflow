#ifndef SECUREFLOW_OP_H_
#define SECUREFLOW_OP_H_

#include <functional>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include "errors.h"
#include "session.h"
#include "macros.h"
#include "shape_inference.h"
#include "types.h"
#include "util.h"

namespace secureflow {

  class OpRegistry;
  class InferenceContext;
  class ExecContext;
  class Op;
  class Session;

  typedef std::function<int(Op*, Session*)> Kernel;
  typedef std::function<int(Op*)> InferenceFn;
  class Op {
  private:

    Kernel debugKernel_;

    Kernel gcKernel_;

    Kernel ssKernel_;

    Kernel gcssKernel_;

    friend class OpBuilder;
  public:
    std::unordered_map<std::string, Tensor*> imap;

    std::unordered_map<std::string, int> smap;

    std::unordered_map<std::string, ValueType> tmap;

    std::vector<Tensor*> ilist;

    Tensor* out;

    Shape* out_shape;

    std::string out_name;

    ValueType out_type;

    InferenceFn inferenceFn;

    int ready_to_run;

    std::string name;

    int is_template;

    Op(){}

    Op(std::string opname) : name(opname), is_template(true) {}

    Op(Op*);

    int set_input(Tensor*, uint32_t idx);

    int get_output(Tensor*&, uint32_t idx);

    inline Kernel debug_kernel() { return debugKernel_; }

    inline Kernel smc_gc_only_kernel() { return gcKernel_; }

    inline Kernel smc_ss_only_kernel() { return ssKernel_; }

    inline Kernel smc_gc_ss_kernel() { return gcssKernel_; }
  };

  class OpRegistry {
  private:
    // op_name -> OpRegistrationData
    std::unordered_map<std::string, Op*> registry_;

  public:

    OpRegistry(){}

    ~OpRegistry();

    Op* lookup(std::string& op_name) const;

    int add(std::string op_name, Op*);

    // Return the global singleton
    static OpRegistry& Global() {
      static OpRegistry instance;
      return instance;
    }

    OpRegistry(OpRegistry const&) = delete;
    void operator=(OpRegistry const&) = delete;

  };

  class OpBuilder {
  public:
    OpBuilder(std::string op_name);
    OpBuilder & Input(std::string input_desc);
    OpBuilder & ListInput(ValueType t);
    OpBuilder & Output(std::string output_desc);
    OpBuilder & Attr(std::string attr, ValueType t);
    OpBuilder & ShapeInferenceFn(InferenceFn);
    OpBuilder & DebugKernel(Kernel);
    OpBuilder & GCOnlyKernel(Kernel);
    OpBuilder & SSOnlyKernel(Kernel);
    OpBuilder & GCSSKernel(Kernel);
    OpBuilder & Finalize();
  private:
    Op* op_;

    /*
     * tmap: Map typename to a list of tensor names
     * Don't be confused by its name, it has different meaning with op_->tmap.
     */
    std::unordered_map<std::string, std::vector<std::string>> tmap;
    void put_tmap(std::string t, std::string s);
  };


#define REGISTER_OP(op_name)                    \
  REGITSER_OP_HELPER(__COUNTER__, op_name)      \

#define REGITSER_OP_HELPER(ctr, op_name)        \
  OpBuilder builder##ctr = OpBuilder(op_name)   \

}

#endif
