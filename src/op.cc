#ifndef SECUREFLOW_OP_H
#define SECUREFLOW_OP_H

#include "op.h"
#include <assert.h>
#include "util.h"

namespace secureflow {

  /******** Op  ***********/
  int Op::set_input(Tensor*, uint32_t idx) {
    throw std::runtime_error("Not implemented");
    return 0;
  }

  int Op::get_output(Tensor*&, uint32_t idx) {
    throw std::runtime_error("Not implemented");
    return 0;
  }

  Op::Op(Op* op_template) { //
    assert(op_template->is_template_);
    imap = op_template->imap;
    smap = op_template->smap;
    tmap = op_template->tmap;
    out_name = op_template->out_name;
    out_type = op_template->out_type;
    inferenceFn = op_template->inferenceFn;
    debugKernel_ = op_template->debugKernel_;
    gcKernel_ = op_template->gcKernel_;
    ssKernel_ = op_template->ssKernel_;
    gcssKernel_ = op_template->gcssKernel_;
    is_template = false;
    ready_to_run = false;
    name = op_template->name;
  }

  /******** OpRegistry ********/
  /*
   * Look up an op, copy it and return.
   * Throw error if template op cannot be found.
   */
  Op* OpRegistry::lookup(std::string& op_name) const {
    if (registry_.find(op_name) == registry_.end()) {
      throw std::runtime_error("Cannot find op with name");
    }
    Op* op = new Op(registry_.find(op_name)->second);
    return op;
  }

  /*
   * Add the op to registry
   */
  int OpRegistry::add(std::string op_name, Op* op) {
    assert(op->is_template);
    if (registry_.find(op_name) != registry_.end()) {
      throw std::runtime_error("op already existed in registry");
    }
    registry_.insert(make_pair(op_name, op));
    return 0;
  }

  /*
   * Delete all template ops
   */
  OpRegistry::~OpRegistry() {
    for (auto it = registry_.begin(); it != registry_.end(); ++it) {
      delete it->second;
    }
  }

  /******** Op Builder **********/
  /*
   * Create a new OpBuilder with op name
   */
  OpBuilder::OpBuilder(std::string op_name)  {
    op_ = new Op(op_name);
  }

  /*
   * Put the tensor name t to the list of type t.
   */
  void OpBuilder::put_tmap(std::string t, std::string s) {
    if (tmap.find(t) == tmap.end()) {
      std::vector<std::string> vec; vec.push_back(s);
      tmap.insert(make_pair(t, vec));
    } else {
      tmap[t].push_back(s);
    }
  }

  /*
   * Set up a single input port.
   * 'in_desc' contains the input tensor's name and type.
   * This is not the function to call to setup an array of inputs,
   * instead, use ListInput.
   */
  OpBuilder & OpBuilder::Input(std::string in_desc) {
    // If no input with the given name is created,
    // insert to tmap 'name' : 'type', where 'type' is
    // the placeholder for the real ValueType.
    std::vector<std::string> parts = str_split(in_desc, ":");
    std::string name = parts[0];
    std::string type = parts[1];
    assert(parts.size() == 2);
    if (op_->imap.find(name) != op_->imap.end()) {
      throw std::runtime_error("input name already used.");
    }

    put_tmap(type, name);

    return *this;
  }

  /*
   * Set up the output port.
   */
  OpBuilder & OpBuilder::Output(std::string out_desc) {
    std::vector<std::string> parts = str_split(out_desc, ":");
    assert(parts.size() == 2);
    std::string name = parts[0];
    std::string type = parts[1];
    if (op_->out_name == parts[0] || op_->tmap.find(name) != op_->tmap.find(name)) {
      throw std::runtime_error("output tensor name already used.");
    }
    op_->out_name = parts[0];

    put_tmap(type, name);

    return *this;
  }

  /*
   * Given an attribute an value of ValueType
   */
  OpBuilder & OpBuilder::Attr(std::string attr, ValueType t) {
    if (tmap.find(attr) == tmap.end()) {
      throw std::runtime_error("Cannot find attr");
    }
    for (auto it = tmap[attr].begin(); it != tmap[attr].end(); ++it) {
      if (op_->out_name == *it) {
        op_->out_type = t; continue;
      }
      if (op_->imap.find(*it) == op_->imap.end()) {
        throw std::runtime_error("name in tmap but not in imap");
      }
      op_->tmap.insert(make_pair(*it, t));
    }

    return *this;
  }

  OpBuilder & OpBuilder::ShapeInferenceFn(InferenceFn fn) {
    op_->inferenceFn = fn;
    return *this;
  }

  OpBuilder & OpBuilder::DebugKernel(Kernel kernel) {
    op_->debugKernel_ = kernel;
    return *this;
  }

  OpBuilder & OpBuilder::GCOnlyKernel(Kernel kernel) {
    op_->gcKernel_ = kernel;
    return *this;
  }

  OpBuilder & OpBuilder::SSOnlyKernel(Kernel kernel) {
    op_->ssKernel_ = kernel;
    return *this;
  }

  OpBuilder & OpBuilder::GCSSKernel(Kernel kernel) {
    op_->gcssKernel_ = kernel;
    return *this;
  }

  /*
   * Add the op to OpRegistry
   */
  OpBuilder & OpBuilder::Finalize() {
    op_->is_template = true;
    OpRegistry::Global().add(op_->name, op_);
    return *this;
  }

  /*
   * Either unspecified homogeneous inputs, or specified inputs arbitrary type
   * Cannot have both
   */
  REGISTER_OP("AddN")
  .Input("inputs : N * T")
  .Output("sum : T")
  .Attr("T", f32)
  .ShapeInferenceFn([](Op* op)->int {
      if (op->ilist.size() > 1) {
        for (uint i = 0; i < op->ilist.size()-1; i++) {
          assert(*op->ilist[i]->shape() == *op->ilist[i+1]->shape());
        }
      }
      op->out_shape = new Shape(*op->ilist[0]->shape());
      return 0;
    })
  .DebugKernel([](Op* op, Session* sess)->int {
      op->out = new Tensor();
      op->out->create(op->ilist[0]->shape(), op->out_type);
      if (op->ilist.size() > 1) {
        for (uint i = 0; i < op->ilist.size(); i++) {
          if (op->out->aggregateFrom(op->ilist[i]) != 0) {
            throw std::runtime_error("tensor aggregation failed.");
          }
        }
      } else {
        if (op->out->movefrom(op->ilist[0]) != 0) {
          throw std::runtime_error("tensor move failed.");
        }
      }
      return 0;
    })
  .GCOnlyKernel([](Op* op, Session* sess)->int {
      op->out = new Tensor();
      op->out->create(op->ilist[0]->shape(), op->out_type);     // Initialize to 0
      SMCManager* smc_manager = sess->smc_manager;

      // Not a terribly efficient way of doing it, invoke smc manager n times
      if (op->ilist.size() > 1) {
        for (uint i = 0; i < op->ilist.size(); i++) {
          if (smc_manager->gc_aggregate_2d_tensor(op->out, op->ilist[i], op->out) != 0) {
            throw std::runtime_error("tensor aggregation failed.");
          }
        }
      } else {
        if (op->out->movefrom(op->ilist[0]) != 0) {
          throw std::runtime_error("tensor move failed.");
        }
      }
    })
  .SSOnlyKernel([](Op* op, Session* sess)->int {
      op->out = new Tensor();
      op->out->create(op->ilist[0]->shape(), op->out_type);
      SMCManager* smc_manager = sess->smc_manager;

      if (op->ilist.size() > 1) {
        for (uint i = 0; i < op->ilist.size(); i++) {
          if (smc_manager->ss_aggregate_2d_tensor(op->out, op->ilist[i], op->out) != 0) {
            throw std::runtime_error("tensor aggregation failed.");
          }
        }
      } else {
        if (op->out->movefrom(op->ilist[0]) != 0) {
          throw std::runtime_error("tensor move failed.");
        }
      }
    })
  .GCSSKernel([](Op* op, Session* sess)->int {
      op->out = new Tensor();
      op->out->create(op->ilist[0]->shape(), op->out_type);
      SMCManager* smc_manager = sess->smc_manager;

      // Addition is totally linear, should require gc.
      if (op->ilist.size() > 1) {
        for (uint i = 0; i < op->ilist.size(); i++) {
          if (smc_manager->ss_aggregate_2d_tensor(op->out, op->ilist[i], op->out) != 0) {
            throw std::runtime_error("tensor aggregation failed.");
          }
        }
      } else {
        if (op->out->movefrom(op->ilist[0]) != 0) {
          throw std::runtime_error("tensor move failed.");
        }
      }
    })
  .Finalize();

  REGISTER_OP("MatMul")
  .Input("in0 : T")
  .Input("in1 : T")
  .Output("output : T")
  .Attr("T", f32)
  .ShapeInferenceFn([](Op *op) {
      if (op->imap.find("in0") == op->imap.end()) {
        throw std::runtime_error("Cannot find input name in0 in imap");
      }
      if (op->imap.find("in1") == op->imap.end()) {
        throw std::runtime_error("Cannot find input name in1 in imap");
      }
      Shape* s0 = op->imap["in0"]->shape();
      Shape* s1 = op->imap["in1"]->shape();
      int rank_in0 = s0->rank();
      int rank_in1 = s1->rank();

      // Verify Dimension
      // e.g.
      // X: 10x4x30, Y: 30x16 =====> Z:10x4x16
      // X: 3136, Y: 3136x1024 =====> Z:1024

      assert(s0->dim_at(rank_in0 - 1) == s1->dim_at(0));

      op->out_shape = new Shape();

      for (int i = 0; i < rank_in0 - 1; ++i) {
        op->out_shape->add_dim(s0->dim_at(i));
      }
      for (int i = 1; i < rank_in1; ++i) {
        op->out_shape->add_dim(s1->dim_at(i));
      }
      return 0;
    })
  .Finalize();

  // REGISTER_OP("MatMulBatch")
  // .Input("input0 : T")
  // .Input("input1 : T")
  // .Output("output : T")
  // .Attr("T: {fix32}")
  // .ShapeInferenceFn([](Op *op) {
  //         // Batch MatMul
  //         // e.g.
  //         // X: 10000x10x4x30, Y:10000x30x16 ====> Z:10000x10x4x16
  //         ShapeHandle in0 = ctx->get_input(0);
  //         ShapeHandle in1 = ctx->get_input(1);
  //         int rank_in0 = in0->rank();
  //         int rank_in1 = in1->rank();

  //         // Verify batch size
  //         SF_ASSERT(in0->dim_at(0) == in1->dim_at(0)) \
  //             << "Batch size of " << in0->debugString()
  //             << "does not match with " << in1->debugString();

  //         // Compute Shape
  //         ShapeHandle out = new ShapeHandle();

  //         SF_REQUIRES_OK(ctx->subShape(in0, 0, rank_in0-1, out));
  //         SF_REQUIRES_OK(ctx->subShape(in1, 2, rank_in1-1, out));

  //         SF_REQUIRES_OK(ctx->set_output(0, out));
  //         return Status::OK();
  //     })
  // .Finalize();

  // REGISTER_OP("Relu")
  // .Input("input : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .ShapeInferenceFn(shape_inference::UnchangedShape)
  // .Finalize();

  // REGISTER_OP("ReluSoftMax")
  // .Input("input : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .ShapeInferenceFn(shape_inference::UnchangedShape)
  // .Finalize();

  // REGISTER_OP("LinearSoftMax")
  // .Input("input : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .ShapeInferenceFn(shape_inference::UnchagedShape)
  // .Finalize();

  // REGISTER_OP("CrossEntropy")
  // .Input("input0 : T")
  // .Input("input1 : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .ShapeInferenceFn([](InferenceContext* ctx){
  //         // Compute Cross entropy
  //         // Requires both inputs to be rank 1

  //         ShapeHandle in0 = ctx->get_input(0);
  //         ShapeHandle in1 = ctx->get_input(1);

  //         // Verification of ranks and dimensions
  //         SF_ASSERT(in0->rank() == 1) \
  //             << "Input 0 should have rank 1, but has " << in0->rank();
  //         SF_ASSERT(in1->rank() == 1) \
  //             << "Input 1 should have rank 1, but has " << in1->rank();

  //         SF_ASSERT(in0->dim_at(0) == in1->dim_at(0))\
  //             << "Inputs should have same dimensions, but has "
  //             << in0->dim_at(0) << " and " << in1->dim_at(1);

  //         return shape_inference::ScalarShape(ctx);
  //     })
  // .Finalize();

  // REGISTER_OP("ArgMax")
  // .Input("input : T")
  // .Input("dimension : Tidx")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .Attr("Tidx : {int32, int64}")
  // .ShapeInferenceFn(shape_inference::ArgOpShape)
  // .Finalize();

  // REGISTER_OP("ArgMin")
  // .Input("input : T")
  // .Input("dimension : Tidx")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .Attr("Tidx : {int32, int64}")
  // .ShapeInferenceFn(shape_inference::ArgOpShape)
  // .Finalize();

  // REGISTER_OP("Mean")
  // .Input("input : T")
  // .Input("dimension : Tidx")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .Attr("Tidx : {int32, int64}")
  // .ShapeInferenceFn(shape_inference::ReductionShape)
  // .Finalize();

  // REGISTER_OP("Flat")
  // .Input("input : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .ShapeInferenceFn([](InferenceContext* ctx){
  //         // Flatten the tensor
  //         // E.g.
  //         // X : 10x20x30 ====> Z: 6000
  //         // Scalar value has rank 0
  //         ShapeHandle in = ctx->get_input(0);
  //         int in_rank = in->rank();
  //         SF_ASSERT(in_rank > 0) << "Input has rank 0.";

  //         // Compute shape
  //         ShapeHandle out = new ShapeHandle();
  //         SF_REQUIRES_OK(ctx->Flatten(in, out));

  //         SF_REQUIRES_OK(ctx->set_output(0, out));
  //         return Status::OK();
  //     })


  // REGISTER_OP("Convolution2D")
  // .Input("input : T")
  // .Output("output : T")
  // .Attr("T : {fix32}")
  // .Attr("filter : list(int)")
  // .Attr("stride : list(int)")
  // .Attr("padding : string")
  // .ShapeInferenceFn([](InferenceContext* ctx){
  //         // Compute Convolution2D
  //         // Use NHWC format
  //         // For SAME padding, p = ceil(w/s)
  //         // For VALID padding, p = floor((w-f+s)/s)
  //         // Where p is the output patch size, w is width, s is
  //         // stride size and f is kernel size

  //         // Verify padding
  //         StringPiece padding;
  //         SF_ASSERT(ctx->getAttr("padding", &padding).ok())\
  //             << "Unable to get attr: padding";
  //         SF_ASSERT((padding == "SAME") || (padding == "VALID"))\
  //             << "Unsupported padding scheme: " << padding;

  //         // Verify input tensor rank == 4
  //         ShapeHandle input = ctx->get_input(0);
  //         SF_ASSERT(input->rank() == 4)\
  //             << "Input tensor rank should be 4, but is " << input->rank();
  //         int batchSize = input->dim_at(0);
  //         int height = input->dim_at(1);
  //         int width = input->dim_at(2);
  //         int channel = input->dim_at(3);

  //         // Verify filter size == 4 and fInChannel = channel
  //         vector<int> filter;
  //         SF_ASSERT(ctx->getAttr("filter", &filter).ok())\
  //             << "Unable to get attr: filter";
  //         SF_ASSERT(filter.size() == 4)\
  //             << "Filter/kernel size should be 4, but is " << filter.size();
  //         SF_ASSERT(filter[2] == channel)\
  //             << "InChannel does not match with input, input has channel "\
  //             << channel << ", but filter has InChannel " << filter[2];
  //         int fHeight = filter[0];
  //         int fWidth = filter[1];
  //         int fInChannel = filter[2];
  //         int fOutChannel = filter[3];

  //         // Verify stride size == 4 and stride[0] == stride[3] == 1
  //         vector<int> stride;
  //         SF_ASSERT(ctx->getAttr("stride", &stride).ok())\
  //             << "Unable to get attr: stride";
  //         SF_ASSERT(stride.size() == 4)\
  //             << "Stride/kernel size should be 4, but is " << stride.size();
  //         SF_ASSERT(stride[0] == 1)\
  //             << "stride[0] should be equal to 1, but is " << stride[0];
  //         SF_ASSERT(stride[3] == 1)\
  //             << "stride[3] should be equal to 1, but is " << stride[3];
  //         int sHeight = stride[1];
  //         int sWidth = stride[2];

  //         // Compute Shape
  //         ShapeHandle out = new ShapeHandle();
  //         if (padding == 'SAME') {
  //             out.addDimension(batchSize);
  //             out.addDimension(int(ceil(height/sHeight)));
  //             out.addDimension(int(ceil(width/sWidth)));
  //             out.addDimension(fOutChannel);
  //         } else {
  //             out.addDimension(batchSize);
  //             out.addDimension(int(floor((height - fHeight + sHeight)/sHeight)));
  //             out.addDimension(int(floor((width - fWidth + sWidth)/sWidth)));
  //             out.addDimension(fOutChannel);
  //         }
  //         SF_REQUIRES_OK(ctx->set_output(0, out));
  //         return Status::OK();
  //     })
  // .Finalize();

}

#endif //SECUREFLOW_OP_H
