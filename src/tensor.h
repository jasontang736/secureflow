#ifndef SECUREFLOW_TENSOR_H_
#define SECUREFLOW_TENSOR_H_

#include <vector>
#include <memory>

namespace secureflow {

  typedef unsigned char byte;
  typedef std::vector<uint32_t> CellIndex;
  class Triplet;

  enum ValueType {
    f32,
    u32,
    i32,
    u8,
  };

  class Shape {
    /*
     * rank is the number of dimensions it has
     */
  private:
    std::vector<uint32_t> dims_;
  public:
    Shape(){}
    Shape(std::vector<uint32_t> dims) : dims_(dims) {}
    inline uint32_t rank() const { return dims_.size(); }
    inline uint32_t dim_at(uint32_t i) const { return dims_[i]; }
    inline void add_dim(uint32_t dim) { dims_.push_back(dim); }
    inline uint32_t size() const {
      uint32_t size = 1;
      for (auto it = dims_.begin(); it != dims_.end(); ++it) {
        size *= *it;
      }
      return size;
    }
    inline void cleanup() {
      dims_.clear();
    }
    bool operator==(const Shape& rhs) const;
  };

  class Tensor {
  private:
    byte* data_;
    Shape* shape_;
    ValueType type_;
    bool dirty_;
  public:
    Tensor();

    /*
     * Create a 1-dimensional tensor with `dimA` elements
     */
    void create(uint32_t dimA);

    /*
     * Create a 2-dimensional tensor (matrix) with desired number of
     * elements.
     */
    void create(uint32_t dimA, uint32_t dimB);

    /*
     * Create a 3-dimensional tensor.
     */
    void create(uint32_t dimA, uint32_t dimB, uint32_t dimC);

    /*
     * Create an `size`-dimensional tensor, all values initialized to 0.
     */
    void create(uint64_t* dims, uint32_t size);

    /*
     * Create the tensor with desired shape and value type
     */
    void create(Shape* s, ValueType type);

    /*
     * Fill tensor with a single value.
     * On a 0-dimensional tensor, it will be its value.
     * On a 1-dimensional tensor, it will be its 1st value.
     * On an N-dimensional tensor, it will be its 1st value on its 1st dimension.
     */
    void fill(int32_t num);

    /*
     * Fill up the tensor in row-first order.
     */
    void fill(int32_t* num, uint32_t size);

    /*
     * Fill up the tensor in bytes in row-first order.
     */
    void fill(byte* data, uint32_t size);

    /*
     * Fill with bytes directly from file
     */
    void fill_from_file(FILE*f, uint64_t size);

    /*
     * Move from tensor (without copy data)
     */
    int movefrom(Tensor* rhs);

    /*
     * Copy from tensor (copy data)
     */
    int copyfrom(Tensor* rhs);

    /*
     * Get value from tensor as 1-dimensional array
     */
    void get(uint32_t pos, byte** b);

    /*
     * Get value from tensor as 2-dimensional array,
     * will cause error if tensor is not 2-dimensional.
     */
    void get(uint32_t posA, uint32_t posB, byte* b);

    /*
     * Get value from tensor as 3-dimensional array,
     * will cause error if tensor is not 3-dimensional.
     */
    void get(uint32_t posA, uint32_t posB, uint32_t posC, byte* b);

    /*
     * Get value from tensor as 4-dimensional array,
     * will cause error if tensor is not 4-dimensional.
     */
    void get(uint32_t posA, uint32_t posB, uint32_t posC, uint32_t posD, byte* b);

    /*
     * Set value for a specific cell in tensor.
     */
    int set(CellIndex& index, uint32_t size, byte* data);

    /*
     * Set value for a specific cell in tensor. (helper for u32)
     */
    int set(CellIndex& index, uint32_t data);

    /*
     * Set value for a specific cell in tensor. (helper for f32)
     */
    int set(CellIndex& index, float data);

    /*
     * Convert the data to protocol buffer
     */
    // int toProto(TensorProto* tensorProto);

    /*
     * Get shape of tensor
     */
    Shape* shape();

    /*
     * Aggregate from another tensor
     */
    int aggregateFrom(Tensor* t);

    /*
     * Set data type, we should also validate the size.
     */
    void set_type(ValueType t);

    /*
     * Delete data and shape
     */
    void cleanup();

    /*
     * Print each value of its type
     */
    void print();

    friend class Triplet;
  };

  /*
   * Triplet contains three equal-sized, 1-dimensional tensors: tu_, tv_, tz_,
   * their element-wisely relationship is: tu_[i]*tv_[i] = tz_[i].
   */
  class Triplet {
  private:
    uint32_t size_;  // number of triplets
    Tensor* tu_;
    Tensor* tv_;
    Tensor* tz_;
  public:
    Triplet(uint32_t); // create tu_, tv_, tz_
    int init(); // init with random values
    int split(Triplet* tri0, Triplet* tri1);
    int fill(int32_t* us, int32_t* vs, int32_t* zs);
    int fill_with_bytes(byte* u, byte* v, byte* z, uint32_t size);
    void getu(uint32_t pos, int32_t& u);
    void getv(uint32_t pos, int32_t& v);
    void getz(uint32_t pos, int32_t& z);
    byte* getubyte();
    byte* getvbyte();
    byte* getzbyte();
    inline uint32_t size() {
      return size_;
    }
  };
}

#endif
