#ifndef SECUREFLOW_OPTIMIZER_H
#define SECUREFLOW_OPTIMIZER_H

namespace secureflow {

  class Optimizer { // Simple optimization method wrapper
  public:
    virtual void optimize(Node* loss_node, bool is_minimize);
  };

  class GradientDescentOptimizer : public Optimizer {
  private:
    float learning_rate_;
  public:
    GradientDescentOptimizer() {}
    // This is exposed to api
    GradientDescentOptimizer(float learning_rate) : learning_rate_(learning_rate) {}
    void optimize(Node* loss_node, bool is_minimize) override;
  }
}


#endif //SECUREFLOW_OPTIMIZER_H
