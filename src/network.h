#ifndef SECUREFLOW_NETWORK_H_
#define SECUREFLOW_NETWORK_H_

#include "status.h"

namespace secureflow {

  class Package;
  class MailMan {
    // The class responsible for delivering shares to the other end
    // Every thread will share this entity
  public:
    MailMan();
    ~MailMan();
    static MailMan* Global();
  private:
    Status deliver(Package pkg);
    MailMan singleton;
  };
}

#endif
