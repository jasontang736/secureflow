#ifndef SECUREFLOW_STATUS_H_
#define SECUREFLOW_STATUS_H_
#include <memory>
#include <string>

namespace secureflow {

     enum Code {
          Internal,
          InvalidArgument,
          MemoryExhausted,
          NetworkCorruption,
          OK,
     };

     class Status {
     public:
          Status() {}
          Status(Code code, std::string msg);

          static Status OK() { return Status(); }

          bool ok() const { return (state_ == NULL); }

          Code code() const {
               return ok() ? Code::OK : state_->code;
          }

          const std::string error_message() const {
               return ok() ? "" : state_->msg;
          }

     private:
          struct State {
               Code code;
               std::string msg;
          };

          std::unique_ptr<State> state_;
     };
}

#endif
