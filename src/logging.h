#ifndef SECUREFLOW_LOGGING_H_
#define SECUREFLOW_LOGGING_H_

#include <sstream>

namespace secureflow {
  const int INFO = 0;
  const int WARNING = 1;
  const int ERROR = 2;
  const int FATAL = 3;

  class LogMessage : public std::basic_ostringstream<char> {
  public:
    LogMessage(const char* fname, int line, int severity);
    ~LogMessage();

  protected:
    void GenerateLogMessage();

  private:
    const char* fname_;
    int line_;
    int severity_;
  };

  class LogMessageFatal : public LogMessage {
  public:
    LogMessageFatal(const char* file, int line);
    ~LogMessageFatal();
  };

#define _SF_LOG_INFO                                              \
  ::secureflow::LogMessage(__FILE__, __LINE__, secureflow::INFO)
#define _SF_LOG_WARNING                                             \
  ::secureflow::LogMessage(__FILE__, __LINE__, secureflow::WARNING)
#define _SF_LOG_ERROR                                             \
  ::secureflow::LogMessage(__FILE__, __LINE__, secureflow::ERROR)
#define _SF_LOG_FATAL                               \
  ::secureflow::LogMessageFatal(__FILE__, __LINE__)

#define LOG(severity) _SF_LOG_##severity

  template <typename T>
  inline const T& GetReferenceableValue(const T& t) {
  return t;
  }
  template <typename T>
  inline T& GetReferenceableValue(T& t) {
    return t;
  }

  template <typename T>
  inline void MakeCheckOpValueString(std::ostream* os, const T& v) {
    (*os) << v;
  }

  template <typename T1, typename T2>
  std::string* MakeCheckOpString(const T1& v1, const T2& v2m,
                                 const char* exprtext);

  class CheckOpMessageBuilder {
  public:
    explicit CheckOpMessageBuilder(const char* exprtext);
    ~CheckOpMessageBuilder();
    std::ostream* ForVar1() { return stream_; }
    std::ostream* ForVar2();
    std::string* NewString();
  private:
    std::ostringstream* stream_;
  };

  template <typename T1, typename T2>
  std::string* MakeCheckOpString(const T1& v1, const T2& v2, const char* exprtext) {
    CheckOpMessageBuilder comb(exprtext);
    MakeCheckOpValueString(comb.ForVar1(), v1);
    MakeCheckOpValueString(comb.ForVar2(), v2);
    return comb.NewString();
  }

#define SF_DEFINE_CHECK_OP_IMPL(name, op)                       \
  template <typename T1, typename T2>                           \
  inline std::string* name##Impl(const T1& v1, const T2& v2,    \
                                 const char* exprtext) {        \
    if (v1 op v2)                                               \
      return NULL;                                              \
    else                                                        \
      return ::secureflow::MakeCheckOpString(v1, v2, exprtext); \
  }                                                             \

  SF_DEFINE_CHECK_OP_IMPL(Check_EQ, ==)
  SF_DEFINE_CHECK_OP_IMPL(Check_NE, !=)
  SF_DEFINE_CHECK_OP_IMPL(Check_LE, <=)
  SF_DEFINE_CHECK_OP_IMPL(Check_LT, <)
  SF_DEFINE_CHECK_OP_IMPL(Check_GE, >=)
  SF_DEFINE_CHECK_OP_IMPL(Check_GT, >)
#undef SF_DEFINE_CHECK_OP_IMPL


#define CHECK_OP_LOG(name, op, val1, val2)                              \
  while (::secureflow::CheckOpString _result =                          \
         ::secureflow::name##Impl(                                      \
                                  ::secureflow::GetReferenceableValue(val1), \
                                  ::secureflow::GetReferenceableValue(val2), \
                                  #val1 " " #op " " #val2))             \
    ::secureflow::LogMessageFatal(__FILE__, __LINE__) << *(_result.str_)
}

#endif
