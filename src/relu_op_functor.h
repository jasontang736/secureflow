#ifndef SECUREFLOW_KERNELS_RELU_OP_FUNCTOR_H_
#define SECUREFLOW_KERNELS_RELU_OP_FUNCTOR_H_

#include "tensor_types.h"

namespace secureflow {
    namespace functor {

        // Functor used by ReluOp to do the computation.
        template <typename Device, typename T>
        struct Relu {
            //Computes Relu activation.
            //
            // features: any shape.
            // activations: same shape as "features".
            void operator()(const Device& d, typename TTypes<T>::ConstTensor features,
                            typename TTypes<T>::Tensor activations) {
                activations.device(d) = features.cwiseMax(static_cast<T>(0));
            }
        };

        // Functor used by ReluGradOp to do the computations.
        template <typename Device, typename T>
        struct ReluGrad {
            // Computes ReluGrad backprops.
            //
            // gradients: gradients backpropagated to the Relu op.
            // features: either the inputs that were passed to the Relu or, or its
            //           outputs (using either one yields the same result here).
            // backprops: gradients to backpropagate to the Relu inputs.
            void operator()(const Device& d, typename TTypes<T>::ConstTensor gradients,
                            typename TTypes<T>::ConstTensor features,
                            typename TTypes<T>::Tensor backprops) {
                // NOTE: When the activation is exactly zero, we do not propagate the
                // associated gradient value. This allows the output of the Relu to be used,
                // as well as its input.
                backprops.device(d) = gradients * (features > static_cast<T>(0)).template cast<T>();
            }
        }
    }
}

#endif
