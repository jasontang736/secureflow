#ifndef SECUREFLOW_THREAD_POOL_H_
#define SECUREFLOW_THREAD_POOL_H_

#include <memory>
#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "status.h"
#include "kernel.h"

namespace secureflow {

    class ThreadPool {
    public:
        ThreadPool(size_t n);

        template <typename KernelType>
        Status enqueue(KernelType* k, OpContext* opCtx, SMCContext *smcCtx);

        ~ThreadPool();
    private:
        std::vector<std::thread> workers_;
        std::queue<function<void()> > bindedReadyKernels_;
        std::mutex queue_mutex_;
        std::condition_variable condition_;
        bool stop_;

        SF_DISALLOW_COPY_AND_ASSIGN(ThreadPool);
    };
}

#endif
