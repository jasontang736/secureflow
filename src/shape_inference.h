#ifndef SECUREFLOW_CORE_FRAMEWORK_SHAPE_INFERENCE_H_
#define SECUREFLOW_CORE_FRAMEWORK_SHAPE_INFERENCE_H_

#include <vector>
#include "tensor.h"
#include "errors.h"
#include "macros.h"

namespace secureflow {

        // All Shape* and Dimension* returned by functions of InferenceContext are owned
        // by the InferenceContext.
        // class InferenceContext {
        // public:
        //     // REQUIRES: <node_def> is not NULL, and must outlive the InferenceContext.
        //     InferenceContext(
        //             const NodeDef* node_def, const OpDef& op_def,
        //             const std::vector<ShapeHandle>& input_shapes,
        //             const std::vector<const Tensor*>& input_tensors);

        //     ~InferenceContext();
        //     // Runs the shape inference function 'fn' with 'this' as the
        //     // argument, returns the status of the inference.
        //     //
        //     // On error, additional context is provided in the error message.
        //     Status Run(const std::function<Status(InferenceContext* c)>& fn);

        //     ShapeHandle get_input(int idx) const { return inputs_[idx]; }
        //     // Status input(StringPiece input_name,
        //     // std::vector<ShapeHandle>* output) const;
        //     int num_inputs() const { return inputs_.size(); }

        //     Status set_output(int idx, ShapeHandle output_shape);

        //     // Append the dimension of inShape from startDim to endDim
        //     // to outShape. (Endpoints inclusive)
        //     Status subShape(ShapeHandle inShape, int startDim, int endDim,
        //                     ShapeHandle outShape);

        //     // Flatten in to out
        //     Status Flatten(ShapeHandle inShape, ShapeHandle outShape);

        //     // Get attribute by name
        //     template <typename T>
        //     Status getAttr(std::string attr_name, T* value);

        // private:
        //     std::vector<ShapeHandle> inputs_;
        //     std::vector<ShapeHandle> outputs_;

        //     SF_DISALLOW_COPY_AND_ASSIGN(InferenceContext);
        // };
}

#endif
