#include <inttypes.h>

typedef unsigned char byte;

byte* secflow_readmnist_image_train_single(int i);
uint8_t secflow_readmnist_label_train_single(int i);

byte* secflow_readmnist_image_test_single(int i);
uint8_t secflow_readmnist_label_test_single(int i);

byte* secflow_readmnist_image_train_batch(int* ids);
uint8_t* secflow_readmnist_label_test_bacth(int* ids);
