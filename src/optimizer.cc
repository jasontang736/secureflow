#include "optimizer.h"
#include "graph.h"

namespace secureflow {
  GradientDescentOptimizer::optimize(Node* loss_node, float learning_rate, bool is_minimize) {
    bool loss_node_visited = false;
    for(auto * iterator = g->nodes_.end(); iterator != g->nodes_.begin();
        ++iterator) {
      // This reverse iteration works because the original graph is a DAG
      Node* n = *iterator;
      if (!loss_node_visited && (n != loss_node))
        continue;

      NodeDef* ndef = n->Def();

      if (n->is_op()) {
        std::string op_name = ndef->op().name();
        std::string op_grad_name = StrCat("grad", op_name);
        OpDef* op_grad_def = new OpDef(OpRegistry::Global()->LookUpDefByName());
        std::shared_ptr<NodeDef> grad_node = std::make_shared<NodeDef>();

        *(grad_node->mutable_op()) = *op_grad_def;
        std::string name = std::string(op_grad_name);
        int64 id;
        g->newNameId(name, id);
        op_grad_def->set_name(name);
        op_grad_def->set_id(id);

        Node* grad_n = g->simpleAddNode(*op_grad_def);

        // The grad node referencing mechanism requires a bit explanation
        // Suppose that there are N nodes, each of which is labelled N_i
        // Now, after we create the gradient version of N_i, how to
        // correctly connect it in the graph?
        //
        // Let's say that the gradient node of N_i is labelled Ng_i.
        // Let's also denote the set of nodes that feeds from the output
        // of N_i (that is, the out_neighbors of N_i) as ON_i.
        // For every node `n` \in ON_i, their gradient node must have been
        // created, or it is not a DAG.
        // We now the gradient node of every `n` \in ON_i, and connect the
        // sum to the 0-th input of Ng_i.
        // Then, we also connect every input of N_i to Ng_i. Done.

        // Handle gradient inputs
        SF_ASSERT(n->out_neighbors().size() > 0);
        if (n->out_neighbors().size() == 1) {
          // No need for AddN node
          Node* out_grad_node = n->out_neighbors[0]->grad_node();
          SF_REQUIRE_NOT_NULL(out_grad_node);
          g->addEdge(out_grad_node, grad_n, 0, 0);
        } else {
          // Create an AddN node to sum all gradient inputs.
          OpDef* addn_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("AddN"));

          std::shared_ptr<NodeDef> addn_nodedef = std::make_shared<NodeDef>();

          *(addn_nodedef->mutable_op()) = *addn_opdef;

          std::string name = std::string("AddN");
          int64 id;
          g->newNameId(name, id);
          addn_nodedef->set_name(name);
          addn_nodedef->set_id(id);

          Node* addn_n = g->simpleAddNode(*addn_nodedef);

          // Sum them up
          int out_neighbor_counter = 0;
          for(auto & iterator = n->out_neighbors();
              iterator != n->out_neighbors().end(); iterator++) {
            Node* out_neighbor = *iterator;
            Node* out_neighbor_grad = out_neighbor->grad_node();
            SF_REQUIRES_NOT_NULL(out_neighbor_grad);
            g->addEdge(out_neighbor_grad, addn_n, 0, out_neighbor_counter++);
          }
          g->addEdge(addn_n, grad_n, 0, 0);
        }

        // Handle original inputs
        int in_neighbor_counter = 1;
        for (auto & iterator = n->in_neighbors();
             iterator != n->in_neighbors().end(); iterator++) {
          Node* in_neighbor = *iterator;
          g->addEdge(in_neighbor, grad_n, 0, in_neighbor_counter++);
        }

        n->set_grad_node(grad_n);
      }
      else if (n->is_variable()) {
        // For every out_neighbors `N_i` of this node, locate its
        // gradient node `Ng_i`, then create an AddN node to sum up
        // all their gradients. Then create a MulConst node to multiply
        // the gradients with positive/negative learning rate, depending
        // on whether this is from a minimize() call or maximize() call.

        // Create apply node
        OpDef* apply_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("Apply"));
        std::shared_ptr<NodeDef> apply_nodedef = std::make_shared<NodeDef>();

        *(apply_nodedef->mutable_op()) = *apply_opdef;

        std::string name = std::string("Apply");
        int64 id;
        g->newNameId(name, id);
        apply_nodedef->set_name(name);
        apply_nodedef->set_id(id);

        Node* apply_n = g->simpleAddNode(apply_nodedef);

        Node* gradient_n;

        SF_ASSERT(n->out_neighbors().size() > 0);
        if (n->out_neighbors().size() == 1) {
          // Directly use the only grad_node of its out_neighbors
          gradient_n = n->out_neighbors()[0]->grad_node();
          SF_REQUIRES_NOT_NULL(gradient_n);
        } else {
          // Create AddN node
          OpDef* addn_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("AddN"));
          std::shared_ptr<NodeDef> addn_nodedef = std::make_shared<NodeDef>();
          *(addn_nodedef->mutable_op()) = *addn_opdef;
          std::string name = std::string("AddN");
          int64 id;
          g->newNameId(name, id);
          addn_nodedef->set_name(name);
          addn_nodedef->set_id(id);

          Node* addn_n = g->simpleAddNode(addn_nodedef);

          int out_neighbor_counter = 0;
          for(auto & iterator = n->out_neighbors();
              iterator != n->out_neighbors().end(); iterator++) {
            Node* out_neighbor = *iterator;
            Node* out_neighbor_grad = out_neighbor->grad_node();

            SF_REQUIRES_NOT_NULL(out_neighbor_grad);
            g->addEdge(out_neighbor_grad, addn_n, 0, out_neighbor_counter++);
          }

          gradient_n = addn_n;
        }
        // Create MultConst node
        OpDef* mult_opdef = new OpDef(OpRegistry::Global()->LookUpDefByName("MultConst"));
        auto* attr_map = mult_opdef->mutable_attr();
        auto iterator = attr_map->find("C");    // The constant
        SF_ASSERT(iterator != attr_map->end());

        // Set learning rate
        AttrValue attr_value;
        iterator->second.set_f(is_minimize ? -learning_rate_ : learning_rate_);

        std::shared_ptr<NodeDef> mult_nodedef = std::make_shared<NodeDef>();
        *(mult_nodedef->mutable_op()) = *mult_opdef;
        std::string name = std::string("MultConst");
        int64 id;
        g->newNameId(name, id);
        mult_nodedef->set_name(name);
        mult_nodedef->set_id(id);

        Node* multc_n = g->simpleAddNode(mult_nodedef);
        g->addEdge(gradient_n, multc_n, 0, 0);

        // Connect to apply node
        g->addEdge(n, apply_n, 0, 0);
        g->addEdge(multc_n, apply_n, 0, 0);

        // Connect apply node to sink node
        g->addEdge(apply_n, g->sink(), 0);
      }
    }
  }
}
