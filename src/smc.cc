/*
  smc.cc -- SMC structs
  author: Xiaoting Tang
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include "smc.h"

namespace secureflow {

    SMCParty::SMCParty(const char* peer_host_name, uint16_t port,
                       const char* client_host_name, uint16_t c_port) {
        role_ = 1;

        // Setup peer sock
        setup_s1_peer(peer_host_name, port);

        // Setup client sock
        setup_client(client_host_name, c_port);

        ready_ = 1;
    }

    SMCParty::SMCParty(uint16_t port, const char* client_host_name, uint16_t c_port) {
        role_ = 0;

        // Setup peer sock
        setup_s0_peer(port);

        // Setup client sock
        setup_client(client_host_name, c_port);

        ready_ = 1;
    }

    int SMCParty::setup_s0_peer(uint16_t port) {

        struct sockaddr_in address;
        int opt = 1;

        if ((self_sock_ = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
            perror("socket failed.");
            exit(1);
        }

        if (setsockopt(self_sock_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof opt) == -1) {
            perror("setsockopt");
            exit(1);
        }

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons( port );

        if (bind(self_sock_, (struct sockaddr*)&address, sizeof(address)) != 0) {
            perror("bind error\n");
            exit(1);
        }


        if (listen(self_sock_, 3) < 0) {
            perror("listen");
            exit(1);
        }

        return 0;
    }

    int SMCParty::setup_s1_peer(const char* peer_host_name, uint16_t port) {

        if ((peer_sock_ = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            fprintf(stderr, "Socket creation error.");
            exit(1);
        }

        memset(&peer_addr_, '0', sizeof(peer_addr_));
        peer_addr_.sin_family = AF_INET;
        peer_addr_.sin_port = htons(port);

        if(inet_pton(AF_INET, peer_host_name, &peer_addr_.sin_addr)<=0)
        {
            printf("\nInvalid address/ Address not supported \n");
            return -1;
        }

        return 0;
    }

    int SMCParty::setup_client(const char* client_host_name, uint16_t port) {

        if ((client_sock_ = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            fprintf(stderr, "Socket creation error.");
            exit(1);
        }

        memset(&client_addr_, '0', sizeof(client_addr_));
        client_addr_.sin_family = AF_INET;
        client_addr_.sin_port = htons(port);

        if(inet_pton(AF_INET, client_host_name, &client_addr_.sin_addr)<=0)
        {
            printf("\nInvalid address/ Address not supported \n");
            return -1;
        }

        return 0;
    }

    /*
     * Connect to peer
     * Return 0 if success, otherwise return 1
     */
    int SMCParty::connect_peer() {
        assert(ready_);
        if (role() == 0) {
            // server
            int addrlen = sizeof(self_addr_);
            if ((peer_sock_ = accept(self_sock_, (struct sockaddr *)&self_addr_,
                                     (socklen_t*)&addrlen))<0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
        } else {
            // client
            if (connect(peer_sock_, (struct sockaddr*)&peer_addr_, sizeof(peer_addr_)) != 0) {
                perror("connect");
                exit(1);
            }
        }
        return 0;
    }

    /*
     * Connect to client
     * Return 0 if success, otherwise return 1
     */
    int SMCParty::connect_client() {
        assert(ready_);
        if (connect(client_sock_, (struct sockaddr*)&client_addr_, sizeof(client_addr_)) != 0) {
            perror("connect");
            exit(1);
        }
        return 0;
    }

    /*
     * Recv a share of integer from client
     */
    int SMCParty::recv_share(int32_t & s) {
        assert(ready_);
        int recv_amt = 0;
        int num_try = 10;
        while (recv_amt < sizeof s && num_try > 0) {
            recv_amt += recv(client_sock_, &s, sizeof s, 0); num_try--;
        }
        if (recv_amt < sizeof s) {
            fprintf(stderr, "Server %d: Error in receiving share from client.\n\
            Expecting recving %d bytes, only recv %d bytes", role(), sizeof s, recv_amt);
            exit(1);
        }
        return 0;
    }

    /*
     * Send a single number to peer
     */
    int SMCParty::send_small(int32_t n) {
        assert(ready_);
        if (tcp_send(peer_sock_, n) != 0) {
            fprintf(stderr, "SMCParty::send_small");
            exit(1);
        }
        return 0;
    }

    /*
     * Recv a triplet from client.
     */
    int SMCParty::recv_triplets(Triplet*& tri) {
        byte* u = NULL;
        byte* v = NULL;
        byte* z = NULL;
        int32_t size = 0;

        if (tcp_recv(client_sock_, &size) != 0) {
            fprintf(stderr, "SMCParty::recv_triplets");
            exit(1);
        }
        assert(size > 0);
        if ((u = tcp_recv(client_sock_)) == NULL) {
            fprintf(stderr, "SMCParty::recv_triplets");
            exit(1);
        }
        if ((v = tcp_recv(client_sock_)) == NULL) {
            fprintf(stderr, "SMCParty::recv_triplets");
            exit(1);
        }
        if ((z = tcp_recv(client_sock_)) == NULL) {
            fprintf(stderr, "SMCParty::recv_triplets");
            exit(1);
        }

        tri = new Triplet(size);
        tri->fill_with_bytes(u, v, z, size);

        return 0;
    }

    /*
     * Recv a single number from peer
     */
    int SMCParty::recv_small(int32_t* n) {
        assert(ready_);
        if (tcp_recv(peer_sock_, n) != 0) {
            fprintf(stderr, "SMCParty::recv_small");
            exit(1);
        }
        return 0;
    }

    SMCClient::SMCClient(uint16_t port) {
        ready_ = 0;
        int opt = 1;

        if ((self_sock_ = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
            perror("socket failed.");
            exit(1);
        }

        if (setsockopt(self_sock_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof opt) == -1) {
            perror("setsockopt");
            exit(1);
        }

        self_addr_.sin_family = AF_INET;
        self_addr_.sin_addr.s_addr = INADDR_ANY;
        self_addr_.sin_port = htons( port );

        if (bind(self_sock_, (struct sockaddr*)&self_addr_, sizeof(self_addr_)) != 0) {
            perror("bind error\n");
            exit(1);
        }


        if (listen(self_sock_, 3) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }

    }

    /*
     * Wait for server0 and server1 to connect.
     * Client does not care the order of connection, it will treat them equally.
     */
    int SMCClient::wait_servers() {

        int addrlen = sizeof(self_addr_);
        if ((server0_sock_ = accept(self_sock_, (struct sockaddr *)&self_addr_,
                                (socklen_t*)&addrlen))<0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if ((server1_sock_ = accept(self_sock_, (struct sockaddr *)&self_addr_,
                                    (socklen_t*)&addrlen))<0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        ready_ = 1;

        return 0;
    }

    /*
     * Send share of an integer to server0 and server1
     */
    int SMCClient::share(int32_t n) {

        int32_t n0 = rand() % (1 << 8);
        int32_t n1 = n-n0;

        if (tcp_send(server0_sock_, n0) != 0) {
            fprintf(stderr, "SMCClient::share error when send to server0");
            exit(1);
        }

        if (tcp_send(server1_sock_, n1) != 0) {
            fprintf(stderr, "SMCClient::share error when send to server1");
            exit(1);
        }

        return 0;
    }

    /*
     * Send multiplicative triplets to server0 and server1
     */
    int SMCClient::send_triplets(uint32_t n) {
        Triplet* tri = new Triplet(n);
        tri->init();
        Triplet* tri0 = new Triplet(n);
        tri0->init();
        Triplet* tri1 = new Triplet(n);
        tri1->init();

        tri->split(tri0, tri1);

        byte* tri0_u = tri0->getubyte();
        byte* tri0_v = tri0->getvbyte();
        byte* tri0_z = tri0->getzbyte();

        byte* tri1_u = tri1->getubyte();
        byte* tri1_v = tri1->getvbyte();
        byte* tri1_z = tri1->getzbyte();

        n = n * sizeof(int32_t);
        // TODO: consider merge u,v,z into a single byte stream
        if (tcp_send(server0_sock_, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server0_sock_, tri0_u, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server0_sock_, tri0_v, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server0_sock_, tri0_z, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }

        if (tcp_send(server1_sock_, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server1_sock_, tri1_u, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server1_sock_, tri1_v, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }
        if (tcp_send(server1_sock_, tri1_z, n) != 0) {
            fprintf(stderr, "Error in SMCClient::send_triplets");
            exit(1);
        }

        return 0;
    }


    /*
     * Send an integer to socket
     * Return 0 if success, return 1 if failed
     */
    int tcp_send(int sock, int32_t n) {
        int numtry = 10;
        int sent_amt = 0;
        while (sent_amt < sizeof n && numtry-- > 0) {
            sent_amt += send(sock, &n + sent_amt, sizeof(n) - sent_amt, 0);
            if (sent_amt < 0) return 1;
        }
        if (sent_amt < sizeof n) return 1;
        return 0;
    }

    /*
     * Recv an integer from socket
     * Return 0 if success, return 1 otherwise
     */
    int tcp_recv(int sock, int32_t* n) {
        int numtry = 10;
        int recv_amt = 0;
        while (recv_amt < sizeof(uint32_t) && numtry-- > 0) {
            recv_amt += recv(sock, n + recv_amt, sizeof(uint32_t) - recv_amt, 0);
            if (recv_amt < 0) return 1;
        }
        if (recv_amt < sizeof(uint32_t)) return 1;
        return 0;
    }

    /*
     * Send bytes of arbitrary length to socket
     * Return 0 if success, return 1 if failed
     */
    int tcp_send(int sock, byte* data, uint32_t size) {
        int numtry = 10;
        int sent_amt = 0;

        // first, send size
        while (sent_amt < sizeof(uint32_t) && numtry-- > 0) {
            sent_amt += send(sock, &size + sent_amt, sizeof(uint32_t) - sent_amt, 0);
            if (sent_amt < 0) return 1;
        }
        if (sent_amt < sizeof(uint32_t)) return 1;

        // next, send date
        numtry = 10; // TODO: consider increase numtry for large size data
        sent_amt = 0;
        while (sent_amt < size && numtry-- > 0) {
            sent_amt += send(sock, data + sent_amt, size - sent_amt, 0);
            if (sent_amt < 0) return 1;
        }
        if (sent_amt < size) return 1;

        return 0;
    }

    /*
     * Recv bytes of arbitrary length from socket
     * Return the pointer to data if success, return NULL if failed
     * `data` will be allocated here, remember to delete it
     */
    byte* tcp_recv(int sock) {
        int numtry = 10;
        int recv_amt = 0;
        uint32_t size = 0;

        while (recv_amt < sizeof(uint32_t) && numtry-- > 0) {
            recv_amt += recv(sock, &size + recv_amt, sizeof(uint32_t) - recv_amt, 0);
            if (recv_amt < 0) return NULL;
        }
        if (recv_amt < sizeof(uint32_t)) return NULL;

        numtry = 10;
        recv_amt = 0;
        byte* data = new byte[size];
        while (recv_amt < size && numtry-- > 0) {
            recv_amt += recv(sock, data + recv_amt, size - recv_amt, 0);
            if (recv_amt < 0) {
                delete [] data;
                return NULL;
            }
        }
        if (recv_amt < size) {
            delete [] data;
            return NULL;
        }

        return data;
    }
}
