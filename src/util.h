#ifndef SECUREFLOW_UTIL_H
#define SECUREFLOW_UTIL_H

#include <string>
#include <vector>

namespace secureflow {

  std::vector<std::string> str_split(std::string s, const char* delim);

}

#endif
