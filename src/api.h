#ifndef SECUREFLOW_API_H
#define SECUREFLOW_API_H

#include <vector>
#include <memory>
#include "session.h"
#include "graph.h"
#include "variable.h"
#include "tensor.h"
#include "smc.h"

namespace secureflow {

  /*
   * Graph related APIs
   */
  Node* secflow_get_source_node(Graph* g);
  Node* secflow_get_sink_node(Graph* g);
  Node* secflow_create_node(Graph* g, string op_name);

  /*
   * Graph computation related API
   */
  Node* secflow_create_placeholder(Graph* g, int shape[], int size);
  Node* secflow_create_variable(Graph* g, int shape[]);
  Node* secflow_create_constant(Graph* g, float v);
  bool secflow_feed_placeholder(Tensor* tensor, Node* n); // will report error if shape does not match
  bool secflow_feed_variable(Tensor* tensor, Node* n);
  Node* secflow_matmul(Node*, Node*); // will report error if shape is invalid.
  Node* secflow_elementwise_matmul(Node*, Node*); // will report error if shape is invalid.
  Node* secflow_square(Node*);
  Node* secflow_sigmoid(Node*);
  Node* secflow_tanh(Node*);
  Node* secflow_sum(Node* ...);
  Node* secflow_backpropagte(Node* y, Node* hy); // return an "Apply" Node
  Node* secflow_argmax(Node*);
  bool secflow_rungraph(Graph* g);
  Tensor* secflow_evaluate(Session* sess, Graph* g, Node* target);

  /*
   * SMC related API
   */
  int secflow_smc_rec(int32_t xi, int32_t& x, SMCParty* peer);
  int secflow_smc_beaver_mul(int32_t x0i, int32_t x1i, Triplet* tri, int32_t& yi, SMCParty* peer);

}

#endif //SECUREFLOW_API_H
