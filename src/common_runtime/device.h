
#ifndef SECUREFLOW_COMMON_RUNTIME_DEVICE_H_
#define SECUREFLOW_COMMON_RUNTIME_DEVICE_H_

#include <memory>
#include <string>

#include "framework/allocator.h"
#include "framework/control_flow.h"
#include "framework/device_attributes.pb_text.h"
#include "framework/device_attributes.pb.h"
#include "framework/device_base.h"
#include "framework/graph.pb.h"
#include "framework/op_kernel.h"
#include "framework/op_segment.h"
#include "framework/resource_mgr.h"
#include "framework/types.h"
#include "graph/graph.h"
#include "graph/types.h"
#include "lib/core/status.h"
#include "platform/macros.h"
#include "platform/types.h"
#include "util/device_name_utils.h"

namespace secureflow {

    class Device : public DeviceBase {
    public:
        Device(Env* env, const DeviceAttributes& device_attributes);
        ~Device() override;

        const string& name() const override { return device_attributes_.name(); }

        // Parsed name of this device
        const DeviceNameUtils::ParsedName& parsed_name() const {
            return parsed_name_;
        }

        const string& device_type() const { return device_attributes_.device_type(); }

        // Returns an aggregation of device attributes.
        const DeviceAttributes& attributes() const override {
            return device_attributes_;
        }

        virtual void Compute(OpKernel* op_kernel, OpKernelContext* context) {
            op_kernel->Compute(context);
        }

        // Asynchronous kernel's compute.
        virtual void ComputeAsync(AsyncOpKernel* op_kernel, OpKernelContext* context,
                                  AsyncOpKernel::DoneCallback done) {
            op_kernel->ComputeAsync(context, std::move(done));
        }

        virtual void ConsumeListOfAccessedTensors(
                DeviceContext* context, const TensorReferenceVector& tensors) {
            for (const auto& ref : tensors) {
                ref.Unref();
            }
        }

        virtual Status Sync() = 0;

        virtual Status MaybeRewriteGraph(std::unique_ptr<Graph>* /*graph*/) {
            return Status::OK();
        }

        virtual Status FillContextMap(const Graph* graph,
                                      DeviceContextMap* device_context_map) {
            return Status::OK();
        }

        OpSegment* op_segment() { return &op_seg_; }

        ResourceMgr* resource_manager() { return rmgr_; }

        string DebugString() const { return ProtoDebugString(device_attributes_); }

        // Assembles the parameter components into a complete DeviceAttributes value.
        static DeviceAttributes BuildDeviceAttributes(
            const string& name, DeviceType device, Bytes memory_limit,
            const DeviceLocality& locality, const string& physical_device_desc);

        static DeviceAttributes BuildDeviceAttributes(
            const string& name, DeviceType device, Bytes memory_limit,
            const DeviceLocality& locality) {
            // Pass in an empty string as physical device name.
            return BuildDeviceAttributes(name, device, memory_limit, locality, "");
        }

    protected:
        void DeleteResourceMgr() {
            delete rmgr_;
            rmgr_ = nullptr;
        }

    private:
        const DeviceAttributes device_attributes_;
        DeviceNameUtils::ParsedName parsed_name_;

        // op_seg_ maps session handle and op name to OpKernel objects.
        OpSegment op_seg_;

        // Resources associated w/ this device. E.g., shared variables, etc.
        ResourceMgr* rmgr_ = nullptr;

        SF_DISALLOW_COPY_AND_ASSIGN(Device);
    };
}
