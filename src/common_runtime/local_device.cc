#include "../common_runtime/local_device.h"

namespace tensorflow {

    bool LocalDevice::use_global_threadpool_ = true;

    struct LocalDevice::ThreadPoolInfo {
        explicit ThreadPoolInfo(const SessionOptions& options) {
            // Undecided
        }
    };

    LocalDevice::LocalDevice(const SessionOptions& options,
                             const DeviceAttributes& attributes)
        : Device(options.env, attributes), owned_tp_info_(nullptr) {
        // Undecided
    }

    LocalDevice::~LocalDevice() {
    }
}
