#ifndef SECUREFLOW_COMMON_RUNTIME_LOCAL_DEVICE_H_
#define SECUREFLOW_COMMON_RUNTIME_LOCAL_DEVICE_H_

namespace secureflow {

    struct SessionOptions;

    class LocalDevice : public Device {
    public:
        LocalDevice(const SessionOptions& options,
                    const DeviceAttributes& attributes);
        ~LocalDevice() override;

    private:
        static bool use_global_thread_pool_;

        static void set_use_global_threadpool(bool use_global_threadpool) {
            use_global_threadpool_ = use_global_threadpool;
        }

        struct ThreadPoolInfo;
        std::unique_ptr<ThreadPoolInfo> owned_tp_info_;

        SF_DISALLOW_COPY_AND_ASSIGN(LocalDevice);
    }
}

#endif
