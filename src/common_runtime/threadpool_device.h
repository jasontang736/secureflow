
#namespace secureflow {

class ThreadPoolDevice : public LocalDevice {
public:
    ThreadPoolDevice(const SessionOptions& options, const string& name,
                     Bytes memory_limit, const DeviceLocality& locality,
                     Allocator* allocator);
    ~ThreadPoolDevice() override;

    void Compute(OpKernel* op_kernel, OpKernelContext* context) override;
    Allocator* GetAllocator(AllocatorAttributes attr) override;
    Status MakeTensorFromProto(const TensorProto& tensor_proto,
                               const AllocatorAttributes alloc_attrs,
                               Tensor* tensor) override;

    Status Sync() override { return Status::OK(); }

private:
    Allocator* allocator_;  // Not owned
};
}
