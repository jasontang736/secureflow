#ifndef SECUREFLOW_COMMON_RUNTIME_FUNCTION_H_
#define SECUREFLOW_COMMON_RUNTIME_FUNCTION_H_

#include <functional>
#include <memory>

#include "common_runtime/device.h"
#include "common_runtime/device_mgr.h"
#include "common_runtime/process_function_library_runtime.h"
#include "framework/function.h"
#include "framework/graph.h"
#include "proptobuf/config.pb.h"

namespace secureflow {

    static constexpr const char* const kNoInlineAttr = "_noinline";

    // Registers a default customizable kernel creator for a function call.
    //
    // If 'cb()' returns a non-OK, we still fall back to an executor-based
    // interpreter op kernel to execute a function. If 'cb()' returns OK,
    // takes ownership of the returned OpKernel.
    //
    // TODO(zhifengc/phawkins): b/32379046
    void RegisterDefaultCustomKernelCreator(CustomKernelCreator cb);

    // Creates a FunctionLibraryRuntime, which instantiates functions
    // defined in "lib_def" and executes functions on the "device".
    // "device_mgr" must contain the "device". If not nullptr,
    // "custom_kernel_creator" is consulted by the returned runtime to
    // create kernels.
    //
    // The returned object does not take ownerships of "device" or
    // "lib_def".  The caller must ensure "device" and "lib_def" outlives
    // the returned object.
    //
    // The "parent" is a pointer to the ProcessFunctionLibraryRuntime object that
    // typically owns the created FunctionLibraryRuntime object. The parent pointer
    // is not owned by the FunctionLibraryRuntime object.
    std::unique_ptr<FunctionLibraryRuntime> NewFunctionLibraryRuntime(
        const DeviceMgr* device_mgr, Env* env, Device* device,
        int graph_def_version, const FunctionLibraryDefinition* lib_def,
        const OptimizerOptions& optimizer_options,
        CustomKernelCreator custom_kernel_creator,
        ProcessFunctionLibraryRuntime* parent);
}
#endif
