class FunctionLibraryRuntimeImpl : public FunctionLibraryRuntime {
 public:
  FunctionLibraryRuntimeImpl(const DeviceMgr* dmgr, Env* env, Device* device,
                             int graph_def_version,
                             const FunctionLibraryDefinition* lib_def,
                             const OptimizerOptions& optimizer_options,
                             CustomKernelCreator custom_kernel_creator,
                             ProcessFunctionLibraryRuntime* parent);

  ~FunctionLibraryRuntimeImpl() override;

  Status Instantiate(const string& function_name, AttrSlice attrs,
                     Handle* handle) override;

  const FunctionBody* GetFunctionBody(Handle handle) override;

  Status CreateKernel(const NodeDef& ndef, OpKernel** kernel) override;

  void Run(const Options& opts, Handle handle, gtl::ArraySlice<Tensor> args,
           std::vector<Tensor>* rets, DoneCallback done) override;

  bool IsStateful(const string& function) override;

  const FunctionLibraryDefinition* GetFunctionLibraryDefinition()
      const override {
    return lib_def_;
  }

  Device* device() override { return device_; }
  Env* env() override { return env_; }
  int graph_def_version() override { return graph_def_version_; }

  string DebugString(Handle h) override;

 private:
  typedef FunctionLibraryRuntimeImpl ME;

  const DeviceMgr* const device_mgr_;
  Device* const device_;
  Env* const env_;
  const int graph_def_version_;
  const FunctionLibraryDefinition* const lib_def_;
  GraphOptimizer optimizer_;
  const CustomKernelCreator custom_kernel_creator_;
  const string device_name_;

  std::function<Status(const string&, const OpDef**)> get_func_sig_;
  std::function<Status(const NodeDef&, OpKernel**)> create_kernel_;

  mutable mutex mu_;

  // func_graphs_ never shrinks or reorders its members.
  std::vector<FunctionBody*> func_graphs_ GUARDED_BY(mu_);

  // The instantiated and transformed function is encoded as a Graph
  // object, and an executor is created for the graph.
  struct Item : public core::RefCounted {
    const Graph* graph = nullptr;  // Owned by exec.
    Executor* exec = nullptr;

    ~Item() override { delete this->exec; }
  };
  std::vector<Item*> items_;

  ProcessFunctionLibraryRuntime* parent_ = nullptr;  // not owned.

  Status FunctionDefToBody(const FunctionDef& fdef, AttrSlice attrs,
                           FunctionBody** fbody);
  Status CreateItem(Handle handle, Item** item);
  Status GetOrCreateItem(Handle handle, Item** item);
  Status InstantiateSymbolicGradient(const NameAttrList& func,
                                     FunctionBody** g_body);
  bool IsLocalTarget(const AttrSlice& attrs);
  AttrValueMap FixAttrs(const AttrSlice& attrs);
  void RunRemote(const Options& opts, Handle handle,
                 gtl::ArraySlice<Tensor> args, std::vector<Tensor>* rets,
                 Executor::Args* exec_args, Item* item, DoneCallback done);

  TF_DISALLOW_COPY_AND_ASSIGN(FunctionLibraryRuntimeImpl);
};

Status FunctionLibraryRuntimeImpl::FunctionDefToBody(const FunctionDef& fdef,
                                                     AttrSlice attrs,
                                                     FunctionBody** fbody) {
    return FunctionDefToBodyHelper(fdef, attrs, lib_def_, get_func_sig_, fbody);
}
Status FunctionLibraryRuntimeImpl::Instantiate(const string& function_name,
                                               AttrSlice attrs,
                                               Handle* handle) {
    AttrValueMap value_map = FixAttrs(attrs);
    AttrSlice new_attrs(&value_map);

    if (!IsLocalTarget(new_attrs)) {
        return parent_->Instantiate(function_name, new_attrs, handle);
    }

    const string key = Canonicalize(function_name, new_attrs);
    *handle = parent_->GetHandle(key);
    if (*handle != kInvalidHandle) {
        return Status::OK();
    }

    Status s;
    FunctionBody* fbody = nullptr;
    if (function_name == kGradientOp) {
        const AttrValue* f = new_attrs.Find(kFuncAttr);
        if (f == nullptr) {
            return errors::InvalidArgument("SymbolicGradient is missing attr: f");
        }
        const auto& func = f->func();
        if (func.name() == kGradientOp) {
            return errors::InvalidArgument("Can't take gradient of SymbolicGradient.");
        }
        const string grad = lib_def_->FindGradient(func.name());
        if (!grad.empty()) {
            return Instantiate(grad, AttrSlice(&func.attr()), handle);
        }
        SF_RETURN_IF_ERROR(InstantiateSymbolicGradient(func, &fbody));
    } else {
        const FunctionDef* fdef = lib_def_->Find(function_name);
        if (fdef == nullptr) {
            return errors::NotFound("Function ", function_name, " is not defined.");
        }
        SF_RETURN_IF_ERROR(FunctionDefToBody(*fdef, new_attrs, &fbody));
    }

    {
        mutex_lock l(mu_);
        *handle = parent_->GetHandle(key);
        if (*handle != kInvalidHandle) {
            delete fbody;
        } else {
            *handle = parent_->AddHandle(key, device_name_, func_graphs_.size());
            func_graphs_.push_back(fbody);
            items_.resize(func_graphs_.size());
        }
    }
}

Status FunctionDefToBodyHelper(
                const FunctionDef& fdef, const AttrSlice& attrs,
                const FunctionLibraryDefinition* const lib_def,
                const std::function<Status(const string&, const OpDef**)>& get_func_sig,
                FunctionBody** fbody) {
    // Instantiates the function template into a graph def.
    InstantiationResult result;
    TF_RETURN_IF_ERROR(InstantiateFunction(fdef, attrs, get_func_sig, &result));

    Graph* graph = new Graph(lib_def);
    GraphConstructorOptions opts;
    opts.allow_internal_ops = true;
    opts.expect_device_spec = false;
    Status s = ConvertNodeDefsToGraph(opts, result.nodes, graph);
    if (!s.ok()) {
        delete graph;
    } else {
        *fbody = new FunctionBody(fdef, result.arg_types, result.ret_types, graph);
    }
    return s;
}

class CallOp : public AsyncOpKernel {
public:
    CallOp(FunctionLibraryRuntime::Handle handle, OpKernelConstruction* ctx)
        : AsyncOpKernel(ctx), handle_(handle) {}

    ~CallOp() override {}

    void ComputeAsync(OpKernelContext* ctx, DoneCallback done) override {
        FunctionLibraryRuntime* lib = ctx->function_library();
        OP_REQUIRES_ASYNC(cts, lib != nullptr,
                          errors::Internal("No function library is provided"),
                          done);
        FunctionLibraryRuntime::Options opts;
        opts.step_id = ctx->step_id();
        opts.rendezvous = ctx->redenzvous();
        opts.cancellation_manager = ctx->cancellation_manager();
        opts.step_container = ctx->step_container();
        opts.stats_collector = ctx->stats_collector();
        opts.runner = ctx->runner();
        std::vector<Tensor> args;
        args.reserve(ctx->num_inputs);
        for (int i = 0; i < ctx->num_inputs(); ++i) {
            args.push_back(ctx->input(i));
        }
        std::vector<Tensor>* rets = new std::vector<Tensor>;
        lib->Run(opts, handle_, args, rets,
                 [ctx, done, rets](const Status& status) {
                     if (!status.ok()) {
                         ctx->SetStatus(status);
                     } else {
                         const int ret_size = static_cast<int>(rets->size());
                         CHECK_EQ(ret_size, ctx->num_outputs());
                         for (int i = 0; i < ret_size; ++i) {
                             ctx->set_output(i, (*rets)[i]);
                         }
                     }
                     delete rets;
                     done();
                 });
    }
}

    FunctionBody* SymbolicGradientHelper::Compute() {
        CHECK(gbody_ == nullptr);
        gbody_ = new FunctionBody;

        // Copy fbody_ into gbody_.
        Copy();

        Graph* g = gbody_->graph;

        const int num_y = static_cast<int>(gbody_->ret_nodes.size());
    }
